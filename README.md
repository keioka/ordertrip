#OrderTrip
![alt text](https://download.unsplash.com/photo-1429966163023-c132bc887fdd "Logo Title Text 1")

## Tool
	* Test: Rspec
	* Code quality: robocop
  * CI integration: Jenkins
  * Benchmarking: http://guides.rubyonrails.org/v3.2.13/performance_testing.html
  * Coding rule: https://github.com/airbnb/ruby#todo-comments(Ruby), https://github.com/airbnb/javascript(Javascript)


## git flow
http://nvie.com/posts/a-successful-git-branching-model/

### master
  - tag

### release
  - 1.x.x
  - 2.x.x

### development
  - merge

### feature
  - implement features
  - merge to development

### fix
  - fix the bug for development & release

### hot fix
  - fix the bug for production

### git config branch.[branch name].mergeoptions "--no-ff"


## API
  * Google Map API
  * Foursquare API
  * Yelp API
  * TripAdvisor API

## Oauth(Omniauth)
  * Twitter
  * Facebook
  * Google
  * Instagram

## User Story

### Non-Users can...
  - See a list of cities
  - Once in city page there are four options we can choose from
    - See a list of tours posted by other travelers ***
    - See a list of tours posted by other tour guides
    - See a list of past tours
    - See a list of tour guides in the city ***
  - See details regarding a tour
    - Traveler basic info
    - Traveler interests
    - Traveler languages
    - Traveler reviews
    - Date
    - Title
    - Location
    - Time Table
    - Description
    - Budget
  - See other users' profiles
    - User's basic info
    - User's interests
    - User's languaes
    - User's reviews
    - User's self-introduction
    - User's records on the site



### Guides can...
  - See the advanced information of the users they've agreed to give tours to
  - Offer to accept tours
    - Accept agreement forms
    - Offer information
    - What they can do
    - Enter credit card information
  - Edit a tour offer (before it's accepted)
  - Delete a tour offer (before it's accepted)

  - See a list of cities
  - Once in city page there are four options we can choose from
    - See a list of tours posted by other travelers ***
    - See a list of tours posted by other tour guides
    - See a list of past tours
    - See a list of tour guides in the city ***
  - See details regarding a tour
    - Traveler basic info
    - Traveler interests
    - Traveler languages
    - Traveler reviews
    - Date
    - Title
    - Location
    - Time Table
    - Description
    - Budget
  - See other users' profiles
    - User's basic info
    - User's interests
    - User's languaes
    - User's reviews
    - User's self-introduction
    - User's records on the site
  - Post tours
    - Date
    - Title
    - Location
    - Time Table
    - Description
    - Budget
    - Credit card information
  - See personal dashboard
    - List of posted tours
    - List of accepted tours
    - List of tour offers they've made
    - Same as seeing other users' profiles
  - Edit Personal Information
  - Edit Tour Post (Only before it is accepted)
  - Delete Personal Account
  - Delete Tour Post



### Travelers can...
  - See a list of cities
  - Once in city page there are four options we can choose from
    - See a list of tours posted by other travelers ***
    - See a list of tours posted by other tour guides
    - See a list of past tours
    - See a list of tour guides in the city ***
  - See details regarding a tour
    - Traveler basic info
    - Traveler interests
    - Traveler languages
    - Traveler reviews
    - Date
    - Title
    - Location
    - Time Table
    - Description
    - Budget
  - See other users' profiles
    - User's basic info
    - User's interests
    - User's languaes
    - User's reviews
    - User's self-introduction
    - User's records on the site
  - Post tours
    - Date
    - Title
    - Location
    - Time Table
    - Description
    - Budget
    - Credit card information
  - See personal dashboard
    - List of posted tours
    - List of accepted tours
    - Same as seeing other users' profiles
  - Edit Personal Information
  - Edit Tour Post (Only before it is accepted)
  - Delete Personal Account
  - Delete Tour Post
  - Register to be a tour guide
    - Legal documentation
  - Accept a tour offer by a tour guide
    - Sign legal documents
    - Confirm their request


## Routes

GET /users/new
GET /users/:id
GET /users/:id/update
POST /users
POST /sessions/create
POST /tours
GET /tours
GET /tours/:id
POST /offer
