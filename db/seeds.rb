# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


# Tour.create(
#   title: "Local food tour inSan Francisco", 
#   description: "Hello, locals!! I would like to eat local foods or famous restaurant! I am 26 years old and interested in foods! Let's go and have good dinner time! :)", 
#   traveler_id: 1, 
#   guide_id: nil, 
#   city_id: nil, 
#   price: 24, 
#   tour_date: 11-24-2015, 
#   start_time: "18:00", 
#   end_time: "20:00", 
#   is_deposited: false, 
#   is_paid: false
# )

# Destination.create(
#   latitude: 37.752845,
#   longitude: -122.509571,
#   address: "1453-1455 Great Hwy
# San Francisco, CA"
# )

# ToursDestination.create(
#   tour_id: 1,
#   destination_id: 1
# )



#Language
languages = ["Abkhazian", "Afar", "Afrikaans", "Akan", "Albanian", "Amharic", "Arabic", "Aragonese", "Armenian", "Assamese", "Avaric", "Avestan", "Aymara", "Azerbaijani", "Bambara", "Bashkir", "Basque", "Belarusian", "Bengali", "Bihari", "Bislama", "Bokmål", "Bosnian", "Breton", "Bulgarian", "Burmese", "Catalan", "Central", "Chamorro", "Chechen", "Chichewa", "Chinese", "Chuvash", "Cornish", "Corsican", "Cree", "Croatian", "Czech", "Danish", "Divehi", "Dutch", "Dzongkha", "English", "Esperanto", "Estonian", "Ewe", "Faroese", "Fijian", "Finnish", "French", "Frisian", "Fulah", "Gaelic", "Galician", "Ganda", "Georgian", "German", "Greek", "Guarani", "Gujarati", "Haitian", "Hausa", "Hebrew", "Herero", "Hindi", "Hiri", "Hungarian", "Icelandic", "Ido", "Igbo", "Indonesian", "Interlingua", "Interlingue", "Inuktitut", "Inupiaq", "Irish", "Italian", "Japanese", "Javanese", "Kalaallisut", "Kannada", "Kanuri", "Kashmiri", "Kazakh", "Khmer", "Kikuyu", "Kinyarwanda", "Kirghiz", "Komi", "Kongo", "Korean", "Kuanyama", "Kurdish", "Lao", "Latin", "Latvian", "Limburgan", "Lingala", "Lithuanian", "Luba-Katanga", "Luxembourgish", "Macedonian", "Malagasy", "Malay", "Malayalam", "Maltese", "Manx", "Maori", "Marathi", "Marshallese", "Mongolian", "Motu", "Nauru", "Navajo", "Ndebele", "Ndebele", "Ndonga", "Nepali", "Northern", "Norwegian", "Norwegian", "Nynorsk", "Occitan", "Ojibwa", "OroOroOroOr", "Ossetian", "Pali", "Panjabi", "Persian", "Polish", "Portuguese", "Pushto", "Quechua", "Romanian", "Romansh", "Rundi", "Russian", "Sami", "Samoan", "Sango", "Sanskrit", "Sardinian", "Serbian", "Shona", "Sichuan", "Sindhi", "Sinhala", "Slavic", "Slovak", "Slovenian", "Somali", "Sotho", "Spanish", "Sundanese", "Swahili", "Swati", "Swedish", "Tagalog", "Tahitian", "Tajik", "Tamil", "Tatar", "Telugu", "Thai", "Tibetan", "Tigrinya", "Tonga", "Tsonga", "Tswana", "Turkish", "Turkmen", "Twi", "Uighur", "Ukrainian", "Urdu", "Uzbek", "Venda", "Vietnamese", "Volapük", "Walloon", "Welsh", "Western", "Wolof", "Xhosa", "Yi", "Yiddish", "Yoruba", "Zhuang", "Zulu", "languages"]


languages.each do |language|
  Language.create(name: language)
end

# 100.times do 

# User.create(
#   first_name: Faker::Name.first_name,
#   last_name: Faker::Name.last_name,
#   username: nil,
#   gender: "1",
#   email: Faker::Internet.free_email,
#   phone_number: nil,
#   is_guide: true,
#   description: "Hi, I am Japanese who worked in a web production company as an assistant director. I quit my job to move to San Francisco and learn programming at dev school. So, from October, I am going to start my life in SF!! 
# I love traveling(adventure), watching movies, going to beach. Recently, I challenge surfing. 
# I am interested in history and culture. My favorite foods are pizza, pizza and pizza.
# I am quiet and calm so I think hosting me is very easy. I have an experience to stay at local people house using couchsurfing.",
#   birthday: "1989-08-28" , 
#   city_id: 1
# )

# end


# 100.times do |i|
#   Tour.create(
#   title: "Local food tour in San Francisco", 
#   description: "Hello, locals!! I would like to eat local foods or famous restaurant! I am 26 years old and interested in foods! Let's go and have good dinner time! :)", 
#   traveler_id: i, 
#   guide_id: nil, 
#   budget: rand(99) + 50,
#   city_id: rand(9)+1, 
#   tour_date: Faker::Date.forward(23).strftime("%F"), 
#   start_time: Faker::Time.forward(23, :morning).strftime("%H:%M"), 
#   end_time: Faker::Time.forward(23, :evening).strftime("%H:%M"), 
#   is_deposited: false, 
#   is_paid: false
# )

# end













