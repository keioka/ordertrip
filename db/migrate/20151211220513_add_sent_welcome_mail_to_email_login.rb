class AddSentWelcomeMailToEmailLogin < ActiveRecord::Migration
  def change
    add_column :email_logins, :sent_welcome_mail, :boolean
  end
end
