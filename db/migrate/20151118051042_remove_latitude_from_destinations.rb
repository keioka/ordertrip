class RemoveLatitudeFromDestinations < ActiveRecord::Migration
  def change
    remove_column :destinations, :latitude, :decimal
  end
end
