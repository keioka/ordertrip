class CreateEmailNotifications < ActiveRecord::Migration
  def change
    create_table :email_notifications do |t|
      t.boolean :welcome_email
      t.boolean :new_tour_notification
      t.references :user
      t.timestamps null: false
    end
  end
end
