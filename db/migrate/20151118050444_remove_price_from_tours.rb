class RemovePriceFromTours < ActiveRecord::Migration
  def change
    remove_column :tours, :price, :integer
  end
end
