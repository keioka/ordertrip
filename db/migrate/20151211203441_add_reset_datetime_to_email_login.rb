class AddResetDatetimeToEmailLogin < ActiveRecord::Migration
  def change
    add_column :email_logins, :reset_datetime, :datetime   
  end
end
