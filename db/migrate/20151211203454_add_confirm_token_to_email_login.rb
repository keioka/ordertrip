class AddConfirmTokenToEmailLogin < ActiveRecord::Migration
  def change
    add_column :email_logins, :confirm_token, :string  
  end
end
