class InitSchema < ActiveRecord::Migration
  def up
    
    create_table "charges", force: :cascade do |t|
      t.integer  "sender_id"
      t.integer  "receiver_id"
      t.integer  "price"
      t.datetime "created_at",  null: false
      t.datetime "updated_at",  null: false
      t.integer  "offer_id"
      t.float    "stripe_fee"
    end
    
    create_table "cities", force: :cascade do |t|
      t.string   "name"
      t.string   "country"
      t.datetime "created_at",      null: false
      t.datetime "updated_at",      null: false
      t.string   "google_place_id"
      t.string   "city_name_id"
      t.string   "state"
    end
    
    add_index "cities", ["city_name_id"], name: "index_cities_on_city_name_id"
    
    create_table "city_lists", force: :cascade do |t|
      t.string   "mc_list_id"
      t.integer  "city_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end
    
    create_table "destinations", force: :cascade do |t|
      t.decimal  "latitude"
      t.decimal  "longitude"
      t.string   "address"
      t.datetime "created_at",        null: false
      t.datetime "updated_at",        null: false
      t.string   "name"
      t.float    "rating"
      t.string   "provider_place_id"
      t.string   "provider"
    end
    
    create_table "email_logins", force: :cascade do |t|
      t.string   "email"
      t.string   "password_digest"
      t.datetime "created_at",      null: false
      t.datetime "updated_at",      null: false
    end
    
    create_table "facebook_logins", force: :cascade do |t|
      t.string   "facebook_uid"
      t.string   "oauth_token"
      t.datetime "oauth_expires_at"
      t.datetime "created_at",       null: false
      t.datetime "updated_at",       null: false
    end
    
    create_table "geolocations", force: :cascade do |t|
      t.decimal  "latitude",         precision: 9, scale: 6
      t.decimal  "longitude",        precision: 9, scale: 6
      t.integer  "geolocation_id"
      t.string   "geolocation_type"
      t.datetime "created_at",                               null: false
      t.datetime "updated_at",                               null: false
    end
    
    add_index "geolocations", ["geolocation_type", "geolocation_id"], name: "index_geolocations_on_geolocation_type_and_geolocation_id"
    
    create_table "google_photo_references", force: :cascade do |t|
      t.string   "reference"
      t.integer  "destination_id"
      t.datetime "created_at",     null: false
      t.datetime "updated_at",     null: false
    end
    
    create_table "image_urls", force: :cascade do |t|
      t.string   "url"
      t.integer  "image_url_id"
      t.string   "image_url_type"
      t.datetime "created_at",     null: false
      t.datetime "updated_at",     null: false
    end
    
    add_index "image_urls", ["image_url_type", "image_url_id"], name: "index_image_urls_on_image_url_type_and_image_url_id"
    
    create_table "languages", force: :cascade do |t|
      t.string   "name"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end
    
    create_table "messages", force: :cascade do |t|
      t.integer  "offer_id"
      t.integer  "sender_id"
      t.integer  "receiver_id"
      t.text     "body"
      t.datetime "created_at",  null: false
      t.datetime "updated_at",  null: false
    end
    
    create_table "offers", force: :cascade do |t|
      t.integer  "guide_id"
      t.integer  "tour_id"
      t.boolean  "accepted"
      t.datetime "accepted_at"
      t.datetime "created_at",  null: false
      t.datetime "updated_at",  null: false
      t.integer  "price"
      t.text     "message"
      t.boolean  "canceled"
      t.datetime "canceled_at"
      t.boolean  "reviewed"
      t.datetime "reviewed_at"
    end
    
    add_index "offers", ["tour_id"], name: "index_offers_on_tour_id"
    
    create_table "reviews", force: :cascade do |t|
      t.integer  "reviewer_id"
      t.integer  "reviewee_id"
      t.string   "title"
      t.text     "body"
      t.float    "rating"
      t.datetime "created_at",  null: false
      t.datetime "updated_at",  null: false
      t.integer  "offer_id"
    end
    
    create_table "tags", force: :cascade do |t|
      t.string   "title"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.string   "title_id"
    end
    
    create_table "tours", force: :cascade do |t|
      t.string   "title"
      t.text     "description"
      t.integer  "traveler_id"
      t.integer  "guide_id"
      t.integer  "city_id"
      t.float    "price"
      t.date     "tour_date"
      t.time     "start_time"
      t.time     "end_time"
      t.boolean  "is_deposited"
      t.boolean  "is_paid"
      t.datetime "created_at",   null: false
      t.datetime "updated_at",   null: false
      t.integer  "budget"
    end
    
    add_index "tours", ["traveler_id"], name: "index_tours_on_traveler_id"
    
    create_table "tours_destinations", force: :cascade do |t|
      t.integer  "destination_id"
      t.integer  "tour_id"
      t.datetime "created_at",     null: false
      t.datetime "updated_at",     null: false
    end
    
    create_table "tours_tags", force: :cascade do |t|
      t.integer  "tour_id"
      t.integer  "tag_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end
    
    create_table "uploaded_images", force: :cascade do |t|
      t.integer  "user_id"
      t.datetime "created_at",         null: false
      t.datetime "updated_at",         null: false
      t.string   "image_file_name"
      t.string   "image_content_type"
      t.integer  "image_file_size"
      t.datetime "image_updated_at"
    end
    
    create_table "users", force: :cascade do |t|
      t.string   "first_name"
      t.string   "last_name"
      t.string   "username"
      t.string   "email"
      t.string   "phone_number"
      t.boolean  "is_guide"
      t.text     "description"
      t.date     "birthday"
      t.datetime "created_at",        null: false
      t.datetime "updated_at",        null: false
      t.string   "password_digest"
      t.integer  "facebook_login_id"
      t.integer  "email_login_id"
      t.integer  "city_id"
      t.integer  "gender"
    end
    
    add_index "users", ["city_id"], name: "index_users_on_city_id"
    
    create_table "users_languages", force: :cascade do |t|
      t.integer  "user_id"
      t.integer  "language_id"
      t.datetime "created_at",  null: false
      t.datetime "updated_at",  null: false
    end
    
    create_table "users_languages_relations", force: :cascade do |t|
      t.integer "user_id"
      t.integer "language_id"
    end
    
    add_index "users_languages_relations", ["language_id"], name: "index_users_languages_relations_on_language_id"
    add_index "users_languages_relations", ["user_id"], name: "index_users_languages_relations_on_user_id"
    
    create_table "users_tags", force: :cascade do |t|
      t.integer  "user_id"
      t.integer  "tag_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end
    
  end

  def down
    raise "Can not revert initial migration"
  end
end
