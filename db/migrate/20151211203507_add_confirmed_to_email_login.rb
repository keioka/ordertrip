class AddConfirmedToEmailLogin < ActiveRecord::Migration
  def change
    add_column :email_logins, :confirmed, :boolean 
  end
end
