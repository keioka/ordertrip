class AddCanceledAtLocalTimeToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :canceled_at_local_time, :DateTime
  end
end
