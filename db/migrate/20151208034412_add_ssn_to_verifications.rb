class AddSsnToVerifications < ActiveRecord::Migration
  def change
    add_column :verifications, :ssn, :boolean
  end
end
