class RemoveLongitudeFromDestinations < ActiveRecord::Migration
  def change
    remove_column :destinations, :longitude, :decimal
  end
end
