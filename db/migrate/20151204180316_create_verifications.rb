class CreateVerifications < ActiveRecord::Migration
  def change
    create_table :verifications do |t|
      t.boolean :facebook
      t.boolean :driver_license
      t.boolean :sex_offender
      t.references :user
      t.timestamps null: false
    end
  end
end
