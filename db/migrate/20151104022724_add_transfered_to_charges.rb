class AddTransferedToCharges < ActiveRecord::Migration
  def change
    add_column :charges, :transfered, :boolean, default: false
  end
end
