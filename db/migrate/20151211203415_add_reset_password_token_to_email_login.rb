class AddResetPasswordTokenToEmailLogin < ActiveRecord::Migration
  def change
    add_column :email_logins, :reset_password_token, :string
  end
end
