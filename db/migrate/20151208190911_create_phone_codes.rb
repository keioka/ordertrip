class CreatePhoneCodes < ActiveRecord::Migration
  def change
    create_table :phone_codes do |t|
      t.string :country
      t.string :code
      t.string :iso
    end
  end
end
