require 'test_helper'

class DashboardsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get tour" do
    get :tour
    assert_response :success
  end

  test "should get transaction" do
    get :transaction
    assert_response :success
  end

end
