require 'rails_helper'

# Response Test
RSpec.describe "StaticPages", type: :request do
  describe "GET /static_pages" do
    it "works! (now write some real specs)" do
      get payment_path
      expect(response).to have_http_status(200)
    end
    it "works! (now write some real specs)" do
      get mission_path
      expect(response).to have_http_status(200)
    end
    it "works! (now write some real specs)" do
      get team_path
      expect(response).to have_http_status(200)
    end
    it "works! (now write some real specs)" do
      get terms_path
      expect(response).to have_http_status(200)
    end
  end
end

# Detail test
describe "/" do
  before do 
    #create a webdriver 
    @driver = Selenium::WebDriver.for :chrome
    #navigate to page
    @driver.navigate.to "http://localhost:3000"
  end

  describe "Top page" do
    let(:signup) { @driver.find_element(:id, "signup").click }
    context "should open popup" do

      xit "should open signup popup" do 
        expect(signup).find_element(:xpath, "//input[@value='Signup']").text.to eq("Signup")
      end

      xit "should open signin popup" do 
        expect(@login).find_element(:xpath, "//input[@value='Signin']").text.to eq("Signup")
      end
    end

    context "login integration test" do
      before do 
        email = EmailLogin.create(
          email: "keioka0828@yahoo.com",
          password: "082889",
          password_confirmation: "082889"
        )
        @driver.find_element(:id, "login").click 
      end
        
      xit "should login" do
        email = EmailLogin.create(
          email: "keioka0828@yahoo.com",
          password: "082889",
          password_confirmation: "082889"
        )
        email = @driver.find_element(:name, 'session[email]') 
        email.send_keys('keioka0828@yahoo.com')

        password = @driver.find_element(:name, 'session[password]') 
        password.send_keys('082889')

        # password_confirmation = @driver.send_keys(:name, 'session[password_confirmation]') 
        # password_confirmation.send_keys('082889')
        
        btn = @driver.find_element(:name, 'login') 
        btn.click

        h2 = @driver.find_element(:tag_name, h2).text
        expect(h2).to eq("Dashboard")
      end

      xit "should not login with empty email" do
        email = @driver.find_element(:name, 'session[email]') 
        email.send_keys('')

        password = @driver.find_element(:name, 'session[password]') 
        password.send_keys('082889')

        btn = @driver.find_element(:name, 'login') 
        btn.click

        alert = @driver.find_element(:class, 'alert-text').text
        expect(alert).to eq("Please check your email & passowrd.")
      end
    end

    xcontext "should signup" do
    end
  end

  after(:each) do 
    @driver.quit
  end
end
