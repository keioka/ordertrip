require 'rails_helper'

# RSpec.describe "Tours", type: :request do
#   describe "GET /tours/new" do
#     it "works! (now write some real specs)" do
#       get new_tour_path
#       expect(response).to have_http_status(200)
#     end
#   end
# end

describe "Tours" do

  # Unit View test ----------------------------------------

  describe "GET /tours/new" do
    xcontext "render right page without current_user" do 
      it "rsponse 200" do
        get "/tours/new"
        expect(response).to have_http_status(500)
      end
    end

    context "render right page with current_user" do 
      it "response 200" do
        get "/tours/new"
        expect(response).to have_http_status(200)
      end

      it "render new" do
        get "/tours/new"
        expect(response).to render_template(:new)
      end
    end
  end

  describe "GET /tours/1" do
    before do 
      #Create tour
    end

    context "render right page with current_user" do 
      xit "response 200" do
        get "/tours/1"
        expect(response).to have_http_status(200)
      end

      xit "render new" do
        get "/tours/1"
        expect(response).to render_template(:show)
      end
    end
  end
  
  describe "GET /tours/1/edit" do
    before do 
      Create tour
    end

    context "render right page with current_user" do 
      xit "response 200" do
        get "/tours/1/edit"
        expect(response).to have_http_status(200)
      end

      xit "render new" do
        get "/tours/1/edit"
        expect(response).to render_template(:edit)
      end
    end
  end

  describe "GET /tours/1" do
    before do 
      Create tour
    end

    context "render right page with current_user" do 
      xit "rsponse 200" do
        get "/tours/1"
        expect(response).to have_http_status(200)
      end

      xit "render new" do
        get "/tours/1"
        expect(response).to render_template(:show)
      end
    end
  end

  # ---------------------------------------- Unit View test End


  # Integration test ----------------------------------------
  
  describe "POST /tours " do
    before do 
      #create a webdriver 
      @driver = Selenium::WebDriver.for :chrome
      #navigate to page
      @driver.navigate.to "http://localhost:3000/tours/new"
    end

    context "City input" do 
      it "should have when input of cityname" do 
        autocomplete = @driver.find_element(:id, "autocomplete")
        autocomplete.send_keys("San Francisco")
        # item = @driver.find_element(:class, "pac-item")
        autocomplete.send_keys(:down)
        # item.sendKeys(Keys.TAB);
      end

      it "should get api info" do
        #get latitude == float
        #get longitude == float
        #get google_place_id == float
      end
    end

    context "Tour input" do
      before do 
        autocomplete = @driver.find_element(:id, "autocomplete")
        autocomplete.send_keys("San Francisco")
        autocomplete.send_keys(:down)
        # item = @driver.find_element(:class, "pac-item")
      end
      it "should get api info" do
        latitude = @driver.find_element(:id, "latitude")
        #get latitude == float
        #get longitude == float
        #get google_place_id == float
      end
    end
  
    after(:all) do 
      @driver.quit
    end
  end

  # ---------------------------------------- Integration test end

end


# post "/widgets", :widget => {:name => "My Widget"}

# response.should redirect_to(assigns(:widget))
# follow_redirect!

# response.should render_template(:show)
# response.body.should include("Widget was successfully created.")