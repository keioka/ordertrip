require 'rails_helper'

describe "UsersController" do
  describe "GET #show" do
    before do 
      email = EmailLogin.create(email: "keioka0828@yahoo.co.jp", password: "000", password_confirmation: "000")
      email.user.update_attributes(
      first_name: "Kei",
      last_name: "Oka",
      username: nil,
      gender: "1",
      email: "keioka0828@yahoo.co.jp",
      phone_number: "00000000",
      is_guide: true,
      description: "Hi, I am Japanese who worked in a web production company as an assistant director. I quit my job to move to San Francisco and learn programming at dev school. So, from October, I am going to start my life in SF!! 
I love traveling(adventure), watching movies, going to beach. Recently, I challenge surfing. 
I am interested in history and culture. My favorite foods are pizza, pizza and pizza.
I am quiet and calm so I think hosting me is very easy. I have an experience to stay at local people house using couchsurfing.",
      birthday: "1989-08-28" , 
      city_id: 1,
      email_login_id: 1
      )
    end
    
    xit "responds successfully with an HTTP 200 status code" do
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end
  
    xit "renders the show template" do
      visit "users/1"
      expect(response).to render_template("show")
    end

    xit "renders the show template" do
      visit "users/1"
      expect(page).to have_content ''
    end
  end

  describe "GET #edit" do
    
  end

  describe "PUT #update" do
    before do 
      email = EmailLogin.create(email: "keioka0828@yahoo.co.jp", password: "000", password_confirmation: "000")
      @user = email.user.update_attributes(
        first_name: "Kei",
        last_name: "Oka",
        username: nil,
        gender: "1",
        email: "keioka0828@yahoo.co.jp",
        phone_number: "00000000",
        is_guide: true,
        description: "Hi, I am Japanese who worked in a web production company as an assistant director. I quit my job to move to San Francisco and learn programming at dev school. So, from October, I am going to start my life in SF!! 
I love traveling(adventure), watching movies, going to beach. Recently, I challenge surfing. 
I am interested in history and culture. My favorite foods are pizza, pizza and pizza.
I am quiet and calm so I think hosting me is very easy. I have an experience to stay at local people house using couchsurfing.",
        birthday: "1989-08-28", 
        city_id: 1,
        email_login_id: 1
      )
      # allow(helper).to receive(:current_user) { @user } 
      ApplicationController.any_instance.stub(:current_user).and_return(@user)
    end
    
    xit "responds successfully with an HTTP 200 status code" do
      put "/users/1", {
        "city" =>{ 
          "name"=>"San Francisco, CA, United States", 
          "latitude"=>"37.7749295", 
          "longitude"=>"-122.41941550000001", 
          "google_place_id"=>"ChIJIQBpAG2ahYAR_6128GcTUEo"
        }, 
        "user_info"=>{
          "email"=>"keioka0828@yahoo.co.jp", 
          "first_name"=>"Kei", 
          "last_name"=>"Oka", 
          "gender"=>"1", 
          "birthday"=>"1989-08-28", 
          "description"=>"dsds"
        }, 
        "languages"=>["Japanese"], 
        "tags"=>["Surfing"]}
    end
  
    xit "renders the show template" do
      PUT "/users/1"
    end

    xit "renders the show template" do
      PUT "/users/1"
    end
  end


end


# describe PostsController do
#   describe 'GET #index' do
#     context 'when user is logged in' do
#       with :user
#       before do
#         sign_in user
#         get :index
#       end
#       it { is_expected.to respond_with :ok }
#       it { is_expected.to respond_with_content_type :html }
#       it { is_expected.to render_with_layout :application }
#       it { is_expected.to render_template :index }
#     end
#     context 'when user is logged out' do
#       before do
#         get :index
#       end
#       it { is_expected.to redirect_to new_session_path }
#       it { is_expected.to set_the_flash(:warning).to('Please log in.') }
#       it { is_expected.to set_session(:return_to).to(posts_path) }
#     end
#   end
# end