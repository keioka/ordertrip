require 'rails_helper'
require 'selenium-webdriver'

describe "ToursController" do
  describe "POST /tours" do
    let!(:user) {
      User.create(
        first_name: "Kei",
        last_name: "Oka",
        username: nil,
        gender: "1",
        email: "keioka0828@yahoo.co.jp",
        phone_number: "00000000",
        is_guide: true,
        description: "Hi, I am Japanese who worked in a web production company as an assistant director. I   quit my job to move to San Francisco and learn programming at dev school. So, from October, I am   going to start my life in SF!! 
          I love traveling(adventure), watching movies, going to beach. Recently, I challenge surfing. 
          I am interested in history and culture. My favorite foods are pizza, pizza and pizza.
          I am quiet and calm so I think hosting me is very easy. I have an experience to stay at local   people house using couchsurfing.",
        birthday: "1989-08-28" , 
        city_id: 1
      )
    }
    let(:destinations_params){
      [
        "ChIJjaIC83F9j4AR1z3B225I_XU", 
        "ChIJI7NivpmAhYARSuRPlbbn_2w", 
        "ChIJ_T25cNd_j4ARehGmHe0pT84", 
        "ChIJabri1qyHhYARLE0Vd4zY_7k"
      ]
    }

    let(:city_params){
      {
        "name"=>"San Francisco, CA, United States", 
        "latitude"=>"37.7749295", 
        "longitude"=>"-122.41941550000001", 
        "google_place_id"=>"ChIJIQBpAG2ahYAR_6128GcTUEo"
      } 
    }

    let(:tour_params){
      {
        "tour_date"=>"2015-10-21", 
        "start_time"=>"10:00", 
        "end_time"=>"18:00", 
        "budget"=>"247", 
        "title"=>"San Francisco", 
        "description"=>"I wanna travel in SF!!"
      }
    }


    before { @current_user = user }

    xit "POST new tour" do
      post "/tours", {:destinations => destinations_params, :city => city_params, :tour => tour_params}
      response.should redirect_to dashboard_path
    end
  end

  describe "GET tours/new" do

    before do 
      visit "tours/new"
    end

    describe "check form element is perfect" do
      context "city info" do 
        it "should have input of city[name]" do
          expect(page).to have_xpath("//input[@name='city[name]']")
        end 

        it "should have input of city[latitude]" do
          expect(page).to have_xpath("//input[@name='city[latitude]']")
        end 

        it "should have input of city[longitude]" do
          expect(page).to have_xpath("//input[@name='city[longitude]']")
        end 

        it "should have input of city[google_place_id]" do
          expect(page).to have_xpath("//input[@name='city[google_place_id]']")
        end 
      end

      context "tour info" do 
        it "should have input of tour[budget]" do
          expect(page).to have_xpath("//input[@name='tour[budget]']")
        end 
        it "should have input of tour[tour_date]" do
          expect(page).to have_xpath("//input[@name='tour[tour_date]']")
        end 

        it "should have input of tour[start_time]" do
          expect(page).to have_xpath("//input[@name='tour[start_time]']")
        end 

        it "should have input of tour[end_time]" do
          expect(page).to have_xpath("//input[@name='tour[end_time]']")
        end 

        it "should have input of tour[title]" do
          expect(page).to have_xpath("//input[@name='tour[title]']")
        end 

        it "should have input of tour[description]" do
          expect(page).to have_xpath("//textarea[@name='tour[description]']")
        end 

        it "should have input of tour[budget]" do
          expect(page).to have_xpath("//input[@name='tour[budget]']")
        end 
      end
    end
  end
end
