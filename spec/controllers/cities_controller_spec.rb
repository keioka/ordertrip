require 'rails_helper'
require 'faker'

describe CitiesController, :type => :controller do
  describe "POST /cities/" do
    context "with valid params" do 
      xit "should be created" do
        post '/cities', {"name"=>"San Francisco, CA, United States", "latitude"=>"37.7749295", "longitude"=>"-122.41941550000001", "google_place_id"=>"ChIJIQBpAG2ahYAR_6128GcTUEo"}
        expect(response).to have_http_status(:success)
      end
    end
  end
  describe "GET /cities/locals_locations" do
    context "with all different city info" do 
      before do 
        10.times do |i|
          User.create(
            first_name: Faker::Name.first_name,
            last_name: Faker::Name.last_name,
            username: nil,
            gender: "1",
            email: Faker::Internet.free_email,
            phone_number: nil,
            is_guide: true,
            description: "Hi, I am Japanese who worked in a web production company as an assistant director. I quit my job to move to San Francisco and learn programming at dev school. So, from October, I am going to start my life in SF!! 
          I love traveling(adventure), watching movies, going to beach. Recently, I challenge surfing. 
          I am interested in history and culture. My favorite foods are pizza, pizza and pizza.
          I am quiet and calm so I think hosting me is very easy. I have an experience to stay at local people house using couchsurfing.",
            birthday: "1989-08-28" , 
            city_id: i + 1
          )
        end
        10.times do |i|
          city = City.create(
            name: i.to_s,
            country: i.to_s,
            google_place_id: i.to_s,
            city_name_id: i.to_s,
          )
          city.create_geolocation(latitude: 10.00, longitude: 10.00)
        end
      end
    
      it "should response 200" do
        get '/cities/locals_locations'
        expect(response.status).to eq 200
      end

      it "should response JSON" do
        get '/cities/locals_locations'
        expect(response.header['Content-Type']).to include 'application/json'
      end

      it "should response 10 objects" do
        get '/cities/locals_locations'
        json = JSON.parse(response.body)
        expect(json.size).to eq 10
      end
    end

    context "with all the same city info" do 
      before do 
        10.times do 
          User.create(
            first_name: Faker::Name.first_name,
            last_name: Faker::Name.last_name,
            username: nil,
            gender: "1",
            email: Faker::Internet.free_email,
            phone_number: nil,
            is_guide: true,
            description: "Hi, I am Japanese who worked in a web production company as an assistant director. I quit my job to move to San Francisco and learn programming at dev school. So, from October, I am going to start my life in SF!! 
          I love traveling(adventure), watching movies, going to beach. Recently, I challenge surfing. 
          I am interested in history and culture. My favorite foods are pizza, pizza and pizza.
          I am quiet and calm so I think hosting me is very easy. I have an experience to stay at local people house using couchsurfing.",
            birthday: "1989-08-28" , 
            city_id: 1
          )
        end
        city = City.create(
          name: "Tokyo",
          country: "Japan",
          google_place_id: "3213",
          city_name_id: "tokyo",
        )
        city.create_geolocation(latitude: 10.00, longitude: 10.00)
      end
    
      it "should response 200" do
        get '/cities/locals_locations'
        expect(response.status).to eq 200
      end

      it "should response JSON" do
        get '/cities/locals_locations'
        expect(response.header['Content-Type']).to include 'application/json'
      end

      it "should response a object" do
        get '/cities/locals_locations'
        json = JSON.parse(response.body)
        expect(json.size).to eq 1
      end
    end

    context "GET /cities/locals_location with city info which doesn't relate with any users. " do 
      before do 
        10.times do 
          User.create(
            first_name: Faker::Name.first_name,
            last_name: Faker::Name.last_name,
            username: nil,
            gender: "1",
            email: Faker::Internet.free_email,
            phone_number: nil,
            is_guide: true,
            description: "Hi, I am Japanese who worked in a web production company as an assistant director. I quit my job to move to San Francisco and learn programming at dev school. So, from October, I am going to start my life in SF!! 
          I love traveling(adventure), watching movies, going to beach. Recently, I challenge surfing. 
          I am interested in history and culture. My favorite foods are pizza, pizza and pizza.
          I am quiet and calm so I think hosting me is very easy. I have an experience to stay at local people house using couchsurfing.",
            birthday: "1989-08-28" , 
            city_id: 1
          )
        end
        city1 = City.create(
          name: "Tokyo",
          country: "Japan",
          google_place_id: "3213",
          city_name_id: "tokyo",
        )
        city1.create_geolocation(latitude: 10.00, longitude: 10.00)

        city2 = City.create(
          name: "Tokyo",
          country: "Japan",
          google_place_id: "3213",
          city_name_id: "tokyo",
        )
        city2.create_geolocation(latitude: 10.00, longitude: 10.00)
      end
    
      it "should response 200" do
        get '/cities/locals_locations'
        expect(response.status).to eq 200
      end

      it "should response JSON" do
        get '/cities/locals_locations'
        expect(response.header['Content-Type']).to include 'application/json'
      end

      it "should response a object and avoid city info without relation with user" do
        get '/cities/locals_locations'
        json = JSON.parse(response.body)
        expect(json.size).to eq 1
      end
    end
  end
end