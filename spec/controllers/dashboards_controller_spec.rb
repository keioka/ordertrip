require 'rails_helper'

RSpec.describe DashboardsController, type: :controller do
  before do
    email = EmailLogin.create(email: "keioka0828@yahoo.co.jp", password: "000", password_confirmation: "000")
    email.user.update_attributes(
      first_name: "Kei",
      last_name: "Oka",
      username: nil,
      gender: 1,
      email: "keioka0828@yahoo.co.jp",
      phone_number: "00000000",
      is_guide: true,
      description: "Hi, I am Japanese who worked in a web production company as an assistant director. I quit my job to move to San Francisco and learn programming at dev school. So, from October, I am going to start my life in SF!! 
I love traveling(adventure), watching movies, going to beach. Recently, I challenge surfing. 
I am interested in history and culture. My favorite foods are pizza, pizza and pizza.
I am quiet and calm so I think hosting me is very easy. I have an experience to stay at local people house using couchsurfing.",
      birthday: "1989-08-28" , 
      city_id: 1,
      email_login_id: 1,
      )
    # @current_user = email.user
    # sign_in(email.user)
  end
  describe "GET /index" do
    context "" do 
      xit "should response success" do
        get '/dashboard'
        expect(response).to be_success
      end

      xit "should response 200" do
        get '/dashboard'
        expect(response.status).to eq 200
      end

      xit "should response 200" do
        get '/dashboard'
        expect(response).to render_template("index")
      end
    end

    context "" do 
     
    end
  end

  describe "GET /order" do
    context "" do 
      xit "should response success" do
        get '/dashboard'
        expect(response).to be_success
      end

      xit "should response 200" do
        get '/dashboard'
        expect(response.status).to eq 200
      end

      xit "should response 200" do
        get '/dashboard'
        expect(response).to render_template("order")
      end
    end

    context "" do 
     
    end
  end

  describe "GET /offer" do
    context "" do 
      xit "should response success" do
        get '/dashboard'
        expect(response).to be_success
      end

      xit "should response 200" do
        get '/dashboard'
        expect(response.status).to eq 200
      end

      xit "should response 200" do
        get '/dashboard'
        expect(response).to render_template("offer")
      end
    end

    context "" do 
    end
  end

  describe "GET /transaction" do
    context "" do 
      xit "should response success" do
        get '/dashboard'
        expect(response).to be_success
      end

      xit "should response 200" do
        get '/dashboard'
        expect(response.status).to eq 200
      end

      xit "should response 200" do
        get '/dashboard'
        expect(response).to render_template("transaction")
      end
    end

    context "" do 
    end
  end
end
