require 'rails_helper'

RSpec.describe ImageUrl, type: :model do

  describe "DB column" do
    it { is_expected.to have_db_column(:url) }

    it { is_expected.to have_db_column(:image_url_id) }

    it { is_expected.to have_db_column(:image_url_type) }
  end

  describe "Validation" do  
  end

  describe "Call back Methods" do
  end

  describe "Class Methods" do
  end

  describe "Instance Methods" do
  end
  
end
