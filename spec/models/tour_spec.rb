require 'rails_helper'

RSpec.describe Tour, type: :model do

  describe "DB column" do 

    it { is_expected.to have_db_column(:title) }

    it { is_expected.to have_db_column(:description) }

    it { is_expected.to have_db_column(:traveler_id) }

    it { is_expected.to have_db_column(:guide_id) }

    it { is_expected.to have_db_column(:city_id) } 

    it { is_expected.to have_db_column(:tour_date) } 

    it { is_expected.to have_db_column(:start_time) } 

    it { is_expected.to have_db_column(:end_time) } 

    it { is_expected.to have_db_column(:is_deposited) } 

    it { is_expected.to have_db_column(:is_paid) } 

    it { is_expected.to have_db_column(:budget) } 

  end

  describe "Accociatrion" do 

    it { is_expected.to have_many(:tours_tags) }

    it { is_expected.to have_many(:tags) }

    it { is_expected.to have_many(:tours_destinations) }

    it { is_expected.to have_many(:destinations) }

    it { is_expected.to have_many(:offers) }
  
    it { is_expected.to belong_to(:city) }

    it { is_expected.to belong_to(:traveler) }
    
  end

  describe "Validation" do
    xcontext "create tour with valid information" do
      let(:tour){Tour.create(params)}
      let(:params) { 
        {  
          title: "Local food tour inSan Francisco", 
          description: "Hello, locals!! I would like to eat local foods or famous restaurant! I am 26 years old and interested in foods! Let's go and have good dinner time! :)", 
          traveler_id: 1, 
          guide_id: nil, 
          city_id: 1, 
          budget: 24, 
          tour_date: "2022-10-25", 
          start_time: "18:00", 
          end_time: "20:00", 
          is_deposited: false, 
          is_paid: false
        } 
      }
      it { expect(tour).to be_valid }
    end

    context "create tour without title" do
      let(:tour){Tour.create(invalide_params)}
      let(:invalide_params) { 
        {  
          title: "", 
          description: "Hello, locals!! I would like to eat local foods or famous restaurant! I am 26 years old and interested in foods! Let's go and have good dinner time! :)", 
          traveler_id: 1, 
          guide_id: nil, 
          city_id: 1, 
          budget: 24, 
          tour_date: "2015-10-25", 
          start_time: "18:00", 
          end_time: "20:00", 
          is_deposited: false, 
          is_paid: false
        } 
      }
      it { expect(tour).to be_invalid }
    end

    context "create tour without description" do
      let(:tour){Tour.create(invalide_params)}
      let(:invalide_params) { 
        {  
          title: "Local food tour inSan Francisco", 
          description: "", 
          traveler_id: 1, 
          guide_id: nil, 
          city_id: 1, 
          budget: 24, 
          tour_date: "2015-10-25", 
          start_time: "18:00", 
          end_time: "20:00", 
          is_deposited: false, 
          is_paid: false
        } 
      }
      it { expect(tour).to be_invalid }
    end

    context "create tour without traveler_id" do
      let(:tour){Tour.create(invalide_params)}
      let(:invalide_params) { 
        {  
          title: "Local food tour inSan Francisco", 
          description: "Hello, locals!! I would like to eat local foods or famous restaurant! I am 26 years old and interested in foods! Let's go and have good dinner time! :)", 
          traveler_id: nil, 
          guide_id: nil, 
          city_id: 1, 
          budget: 24, 
          tour_date: "2015-10-25", 
          start_time: "18:00", 
          end_time: "20:00", 
          is_deposited: false, 
          is_paid: false
        } 
      }
      it { expect(tour).to be_invalid }
    end

    context "create tour without city_id" do
      let(:tour){Tour.create(invalide_params)}
      let(:invalide_params) { 
        {  
          title: "Local food tour inSan Francisco", 
          description: "Hello, locals!! I would like to eat local foods or famous restaurant! I am 26 years old and interested in foods! Let's go and have good dinner time! :)", 
          traveler_id: 1, 
          guide_id: nil, 
          city_id: nil , 
          budget: 24, 
          tour_date: "2015-10-25", 
          start_time: "18:00", 
          end_time: "20:00", 
          is_deposited: false, 
          is_paid: false
        } 
      }
      it { expect(tour).to be_invalid }
    end

    context "create tour with past tour_date" do
      let(:tour){Tour.create(invalide_params)}
      let(:invalide_params) { 
        {  
          title: "Local food tour inSan Francisco", 
          description: "Hello, locals!! I would like to eat local foods or famous restaurant! I am 26 years old and interested in foods! Let's go and have good dinner time! :)", 
          traveler_id: 1, 
          guide_id: nil, 
          city_id: nil , 
          budget: 24, 
          tour_date: "2015-10-05", 
          start_time: "18:00", 
          end_time: "20:00", 
          is_deposited: false, 
          is_paid: false
        } 
      }
      it { expect(tour).to be_invalid }
    end
  end

  describe "Call back Methods" do
  end

  describe "Class Methods" do
  end

  describe "Instance Methods" do
  end
end
