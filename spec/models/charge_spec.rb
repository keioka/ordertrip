require 'rails_helper'

RSpec.describe Charge, type: :model do


  describe "DB column" do 
    it { is_expected.to have_db_column(:sender_id) }

    it { is_expected.to have_db_column(:receiver_id) }

    it { is_expected.to have_db_column(:price) }

    it { is_expected.to have_db_column(:offer_id) }

    it { is_expected.to have_db_column(:stripe_fee) } 
  end


  describe "Accociatrion" do 
    it { is_expected.to belong_to(:offer) }

    it { is_expected.to belong_to(:sender) }

    it { is_expected.to belong_to(:receiver) }
  end


  describe "Validation" do 

    context "should not be created without sender_id" do

      before do 
        @offer = Offer.create(price: 50, message:"Hello")
        @charge = Charge.create(sender_id: nil, receiver_id: 2, offer_id: 1)
      end 

      it { expect(@charge).to be_invalid }
    end

    context "should not be created without receiver_id" do

      before do 
        @offer = Offer.create(price: 50, message:"Hello")
        @charge = Charge.create(sender_id: 1 , receiver_id: nil, offer_id: 1)
      end 

      it { expect(@charge).to be_invalid }
    end

    context "should not be created without offer_id" do

      before do 
        @offer = Offer.create(price: 50, message:"Hello")
        @charge = Charge.create(sender_id: 1 , receiver_id: 2 , offer_id: nil )
      end 

      it { expect(@charge).to be_invalid }
    end

    context "should be created with valid params" do

      before do 
        @offer = Offer.create(price: 50, message:"Hello")
        @charge = Charge.create(sender_id: 1 , receiver_id: 2 , offer_id: 1 )
      end 

      it { expect(@charge).to be_valid }
    end
  end


  describe "Call back Methods" do

    before do 
      @offer = Offer.create(price: 50, message:"Hello")
      @charge = Charge.create(sender_id: 1, receiver_id: 2, offer_id: 1)
    end

    context "def 'subscribtion_stripe_fee' should return right price" do

      it { expect(@charge.price).to eq(50) }

      it { expect(@charge.stripe_fee).to eq(1.5) }

    end

  end


  describe "Class Methods" do
  end


  describe "Instance Methods" do

    before do 
      @offer = Offer.create(price: 50, message:"Hello")
      @charge = Charge.create(sender_id: 1, receiver_id: 2, offer_id: 1)
    end

    context "def 'total_price' should return right price" do
    # let!(:charge){ Charge.create(sender_id: 1, receiver_id: 2, price: 50, offer_id: 3)}
      it { expect(@charge.total_price).to eq(51.5) }

      it { expect(@charge.total_price).not_to eq(50) }

      it { expect(@charge.total_price).not_to eq(1.5) }

    end

  end

end
