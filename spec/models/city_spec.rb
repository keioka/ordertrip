require 'rails_helper'

RSpec.describe City, type: :model do
  describe "DB column" do  
    it { is_expected.to have_db_column(:id) }

    it { is_expected.to have_db_column(:name) }

    it { is_expected.to have_db_column(:country) }

    it { is_expected.to have_db_column(:google_place_id) }

    it { is_expected.to have_db_column(:city_name_id) }
  end

  describe "Accociatrion" do 
    it { is_expected.to have_many(:users) }
  end

  describe "Validation" do 
    city1 = City.create(
      name: "Tokyo",
      country: "Japan",
      google_place_id: "3213"
    )
    xit { is_expected.to validate_uniqueness_of(:city_name_id) } 
  end

  describe "Call back Methods" do
  end

  describe "Class Methods" do
  end

  describe "Instance Methods" do
  end
end
