require 'rails_helper'

RSpec.describe Message, type: :model do
  describe "DB column" do
    it { is_expected.to have_db_column(:offer_id) }

    it { is_expected.to have_db_column(:sender_id) }

    it { is_expected.to have_db_column(:receiver_id) }

    it { is_expected.to have_db_column(:body) }  
  end

  describe "Validation" do  
  end

  describe "Call back Methods" do
  end

  describe "Class Methods" do
  end

  describe "Instance Methods" do
  end
end
