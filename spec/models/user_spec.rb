  require 'rails_helper'

RSpec.describe User, type: :model do
  describe "DB column" do 
    it { is_expected.to have_db_column(:first_name) }

    it { is_expected.to have_db_column(:last_name) }

    it { is_expected.to have_db_column(:username) }

    it { is_expected.to have_db_column(:email) }

    it { is_expected.to have_db_column(:is_guide) }

    it { is_expected.to have_db_column(:description) } 

    it { is_expected.to have_db_column(:phone_number) } 

    it { is_expected.to have_db_column(:birthday) } 

    it { is_expected.to have_db_column(:facebook_login_id) } 

    it { is_expected.to have_db_column(:email_login_id) } 

    it { is_expected.to have_db_column(:city_id) } 

    it { is_expected.to have_db_column(:gender) } 
  end


  describe "Accociatrion" do 
    it { is_expected.to belong_to(:facebook_login) }

    it { is_expected.to belong_to(:email_login) }

    it { is_expected.to belong_to(:city) }

    it { is_expected.to have_many(:charged) }

    it { is_expected.to have_many(:charging) }

    it { is_expected.to have_many(:offers) }

    it { is_expected.to have_many(:users_language) }

    it { is_expected.to have_many(:languages) }

    it { is_expected.to have_many(:users_tag) }

    it { is_expected.to have_many(:tags) }

    it { is_expected.to have_many(:reviews) }

    it { is_expected.to have_many(:reviewers) }
  end

  describe "Validation" do
    let(:email_login_user){EmailLogin.create(params)}
    let(:params) { 
      {  
        email: "keioka0828@gmail.com",
        password: "kei000",
        password_confirmation: "kei000" 
      } 
    }

    context "create user with email_login" do
      it { expect(email_login_user.user).to be_valid}
    end

    context "create user with email_login which has email_login_id" do
      it { expect(email_login_user.user.email_login_id).to be_equal(email_login_user.id)}
    end

    context "create user with phonenumber which is already taken" do
      let(:user){User.create(valid_user_params)}
      let(:valid_user_params) { 
        {  
          first_name: "Kei",
          last_name: "Oka",
          username: "keioka",
          email: "keioka0828us@gmail.com",
          gender: "1",
          phone_number: "415082889",
          is_guide: 1,
          description: "Hello, I am Kei",
          birthday: 2015-1-1,
          facebook_login_id: nil,
          email_login_id: 1,
          city_id: 1
        } 
      }
      it { expect(user).to be_valid}
    end

    context "create user with phonenumber which is already taken" do
      before do 
        User.create(
            first_name: "Kei",
            last_name: "Oka",
            username: "keioka",
            email: "keioka0828us@gmail.com",
            gender: "1",
            phone_number: "415082889",
            is_guide: 1,
            description: "Hello, I am Kei",
            birthday: 2015-1-1,
            facebook_login_id: nil,
            email_login_id: 1,
            city_id: 1
        )
      end
      let(:user2){User.create(invalid_user_params)}
      let(:invalid_user_params) { 
        {  
          first_name: "Kei",
          last_name: "Oka",
          username: "keioka",
          email: "keioka0828us@gmail.com",
          gender: "1",
          phone_number: "415082889",
          is_guide: 1,
          description: "Hello, I am Kei",
          birthday: 2015-1-1,
          facebook_login_id: nil,
          email_login_id: 1,
          city_id: 1
        } 
      }
      it { expect(user2).to be_invalid}
    end
  end

  describe "Call back Methods" do
    context "create verification after user is created" do
    
      let(:user){
        User.create(
          first_name: "Kei",
          last_name: "Oka",
          username: "keioka",
          email: "keioka0828us@gmail.com",
          gender: "1",
          phone_number: "415082889",
          is_guide: 1,
          description: "Hello, I am Kei",
          birthday: 2015-1-1,
          facebook_login_id: nil,
          email_login_id: 1,
          city_id: 1
        )
      }
      it { expect(user.verification).to be_truthy}
    end
  end

  describe "Class Methods" do
  end

  describe "Instance Methods" do
  end

end