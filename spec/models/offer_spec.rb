require 'rails_helper'

RSpec.describe Offer, type: :model do
  describe "DB column" do
    it { is_expected.to have_db_column(:guide_id) }

    it { is_expected.to have_db_column(:tour_id) }

    it { is_expected.to have_db_column(:accepted) }

    it { is_expected.to have_db_column(:accepted_at) }

    it { is_expected.to have_db_column(:price) } 

    it { is_expected.to have_db_column(:message) } 

    it { is_expected.to have_db_column(:canceled) }

    it { is_expected.to have_db_column(:canceled_at) }

    it { is_expected.to have_db_column(:reviewed) }

    it { is_expected.to have_db_column(:reviewed_at) }     
  end

  describe "Validation" do  
  end

  describe "Call back Methods" do
  end

  describe "Class Methods" do
  end

  describe "Instance Methods" do
  end
end
