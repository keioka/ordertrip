require 'rails_helper'

RSpec.describe Geolocation, type: :model do
  describe "DB column" do
    it { is_expected.to have_db_column(:latitude) }

    it { is_expected.to have_db_column(:longitude) }

    it { is_expected.to have_db_column(:geolocation_id) }

    it { is_expected.to have_db_column(:geolocation_type) }
  end

  describe "Validation" do  
  end

  describe "Call back Methods" do
  end

  describe "Class Methods" do
  end

  describe "Instance Methods" do
  end
end
