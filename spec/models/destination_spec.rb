require 'rails_helper'

RSpec.describe Destination, type: :model do
  describe "DB column" do

    it { is_expected.to have_db_column(:address) }

    it { is_expected.to have_db_column(:name) }

    it { is_expected.to have_db_column(:rating) } 

    it { is_expected.to have_db_column(:provider_place_id) } 

    it { is_expected.to have_db_column(:provider) }
  end

  describe "Validation" do
    it { is_expected.to have_many(:image_urls) }

    it { is_expected.to have_many(:google_photo_references) }

    it { is_expected.to have_one(:geolocation) }
  end

  describe "Call back Methods" do
  end

  describe "Class Methods" do
  end

  describe "Instance Methods" do
  end
end
