require 'rails_helper'

RSpec.describe FacebookLogin, type: :model do
  describe "DB column" do
    it { is_expected.to have_db_column(:facebook_uid) }

    it { is_expected.to have_db_column(:oauth_token) }

    it { is_expected.to have_db_column(:oauth_token) } 
  end

  describe "Validation" do  
  end

  describe "Call back Methods" do
  end

  describe "Class Methods" do
  end

  describe "Instance Methods" do
  end
  
end
