require 'rails_helper'

RSpec.describe Review, type: :model do
  describe "DB column" do
    it { is_expected.to have_db_column(:reviewer_id) }

    it { is_expected.to have_db_column(:reviewee_id) }

    it { is_expected.to have_db_column(:title) }

    it { is_expected.to have_db_column(:body) }

    it { is_expected.to have_db_column(:rating) }

    it { is_expected.to have_db_column(:offer_id) }    
  end

  describe "Validation" do  
  end

  describe "Call back Methods" do
  end

  describe "Class Methods" do
  end

  describe "Instance Methods" do
  end
end
