require 'rails_helper'

RSpec.describe ToursDestination, type: :model do
  describe "DB column" do
    it { is_expected.to have_db_column(:destination_id) }

    it { is_expected.to have_db_column(:tour_id) }
  end

  describe "Validation" do  
  end

  describe "Call back Methods" do
  end

  describe "Class Methods" do
  end

  describe "Instance Methods" do
  end
end
