require 'rails_helper'

RSpec.describe UploadedImage, type: :model do
  describe "DB column" do 
    it { is_expected.to have_db_column(:image_file_name) }

    it { is_expected.to have_db_column(:image_content_type) }

    it { is_expected.to have_db_column(:image_file_size) }

    it { is_expected.to have_db_column(:image_updated_at) }

  end

  describe "Accociatrion" do 
    it { is_expected.to belong_to(:user) }
  end
end
