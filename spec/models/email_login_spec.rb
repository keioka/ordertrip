require 'rails_helper'

# RSpec.describe EmailLogin, type: :model do
#   pending "add some examples to (or delete) #{__FILE__}"
# end

RSpec.describe EmailLogin, type: :model do

  describe "DB column" do
    it { is_expected.to have_db_column(:email) }

    it { is_expected.to have_db_column(:password_digest) }
  end

  describe "Validation" do
    let(:email_login_user){EmailLogin.create(params)}
    let(:params){
        {
        email: "keioka0828@gmail.com",
        password: "kei000",
        password_confirmation: "kei000"
      }
    }

    context "create with valid email" do
      email_login_user = EmailLogin.new(
        email: "keioka0828@gmail.com",
        password: "kei000",
        password_confirmation: "kei000"
      )
      it { expect(email_login_user).to be_valid }
    end

    context "create with invalid email (without @ mark)" do
      email_login_user = EmailLogin.new(
        email: "okakeiokaeki.com",
        password: "000",
        password_confirmation: "000"
      )
      it { expect(email_login_user).not_to be_valid }
    end

    context "create with invalid email (without @ mark)" do
      email_login_user = EmailLogin.new(
        email: "okakeiokaeki.com",
        password: "000",
        password_confirmation: "000"
      )
      email_login_user.save
      it {  expect(email_login_user).not_to be_valid}
    end

    context "create with invalid email (without .)" do
      email_login_user = EmailLogin.new(
        email: "keioka@gmailcom",
        password: "000",
        password_confirmation: "000"
      )
      email_login_user.save
       it {  expect(email_login_user).not_to be_valid}
    end


    context "create with empty email" do
      email_login_user = EmailLogin.new(
        email: "",
        password: "000",
        password_confirmation: "000"
      )
      email_login_user.save
       it {  expect(email_login_user).not_to be_valid}
    end

    context "create with empty password" do
      email_login_user = EmailLogin.new(
        email: "keioka@gmailcom",
        password: "",
        password_confirmation: "000"
      )
      email_login_user.save
       it {  expect(email_login_user).not_to be_valid}
    end

    context "create with empty password confirmation" do
      email_login_user = EmailLogin.new(
        email: "keioka@gmailcom",
        password: "000",
        password_confirmation: ""
      )
      email_login_user.save
       it {  expect(email_login_user).not_to be_valid}
    end

    context "create with empty both of paasword & password confirmation" do
      email_login_user = EmailLogin.new(
        email: "keioka@gmailcom",
        password: "",
        password_confirmation: ""
      )
      email_login_user.save
       it {  expect(email_login_user).not_to be_valid}
    end

    context "create with empty all information" do
      email_login_user = EmailLogin.new(
        email: "",
        password: "",
        password_confirmation: ""
      )
      email_login_user.save
       it {  expect(email_login_user).not_to be_valid}
    end

    context "create email_login with email address which is already taken" do
      before do  
        email_login_user
      end
      let(:email_login_user2){EmailLogin.create(params)}
      it { expect(email_login_user2).to be_invalid}
    end
  end

  describe "Call back Methods" do
  end

  describe "Class Methods" do
  end

  describe "Instance Methods" do
  end
end
