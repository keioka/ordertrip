require 'rails_helper'

RSpec.describe ToursTag, type: :model do
  describe "DB column" do 
    it { is_expected.to have_db_column(:tour_id) }

    it { is_expected.to have_db_column(:tag_id) }
  end

  describe "Validation" do  
  end

  describe "Call back Methods" do
  end

  describe "Class Methods" do
  end

  describe "Instance Methods" do
  end
end
