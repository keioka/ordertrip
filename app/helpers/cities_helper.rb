module CitiesHelper
  def image_url(destination)
    if destination.nil?
      url = create_image_assets_path("notfound.jpg")
    elsif destination.provider == "Google"
      begin
        reference = destination.google_photo_references.shuffle.pop.reference
      rescue 
        reference = nil
      end
      if reference
        url = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=600&photoreference=" + reference  + " +&key=AIzaSyAfEZc3f_CMA9cR1TFh-dIKtUYu_fRo44w"
      else
        url = create_image_assets_path("notfound.jpg") 
      end
    elsif destination_has_any_photos?(destination)
      url = create_image_assets_path("notfound.jpg")
    end
    return url
  end

  def create_image_assets_path(file_name)
    ActionController::Base.helpers.asset_path(file_name)
  end

  def tour_destinations_photo(tour)
    image_url(tour.destinations[0])
  end

end
