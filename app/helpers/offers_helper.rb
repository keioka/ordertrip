module OffersHelper

  def find_offer(offer_id)
    @offer = Offer.find(offer_id)
  rescue ActiveRecord::RecordNotFound => e
    logger.warn(e.class, e.messages, e.backtrace)
  end

  def find_offer_from_tour(offer)
    offer.try(:tour)
  end

  def accepted?(offer)
    offer.accepted? == true
  end

  def check_tour_end_from_offer?(offer)
    tour = offer.tour
    tour_ended?(tour)
  end

  def acceptable?(offer)
    #current_user should be guide or traveler | guide?(offer) traveler?(offer)
    # offer should not be accepted
    # tour date should not be passed
    traveler?(offer) && !accepted?(offer) && !check_tour_end_from_offer?(offer) ? true : false
  end

  def canceled?(offer)
    offer.canceled == true
  end

  def reviewed?(offer)
    offer.reviews.each {|review| return true if review.reviewer_id == current_user.id}
    return false
  end

  def print_status_class_offer(offer)
    if canceled?(offer) && accepted?(offer)
      "canceled" 
    elsif accepted?(offer)
      "accepted"
    else
      "pending"
    end
  end

  def reviewable?(offer)
    #current_user should be guide or traveler | guide?(offer) traveler?(offer)
    #offer is accepted | accepted?(offer)
    #offer is not canceled | canceled?(offer)
    #tour date is passed | 
    if guide?(offer) || traveler?(offer)
      accepted?(offer) && check_tour_end_from_offer?(offer) ? true : false
    end
  end

  def guide?(offer)
    offer.guide_id == current_user.id
  end

  def traveler?(offer)
    offer.tour.traveler_id == current_user.id
  end

end
