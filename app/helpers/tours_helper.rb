module ToursHelper
  
  def offered?(tour)
    tour.offers.find_by(guide_id:current_user.id).nil? if current_user
  end

  def order_status(order) 
    offers = order.offers
    if offers.count == 0
      "A"
    elsif offers.any?{|offer| offer.accepted == true }
      "B"
    elsif offers.all?{|offer| offer.accepted == false }
      "C"
    elsif order.tour_date < DateTime.now
      "D"
    end
  end

  def print_status_class(order)
    case order_status(order)
    when "A"
      "no-offer"
    when "B"
      "accepted"
    when "C"
      "offered"
    when "D"
      "done"
    end
  end

  def has_any_offers?(order) #tour
    order_status(order) == "B" || order_status(order) == "C" 
  end

  def editable?(order) #tour
    order_status(order) == "A" || order_status(order) == "C" 
  end

  def tour_ended?(tour)
    city, tour_datetime = get_city(tour), tour_start_date_time(tour) 
    local_current_datetime = local_time_now(city) 
    tour_datetime < local_current_datetime
  end

  def get_city(tour)
    city = tour.city
  end

  def cancel_fee?(tour)
    city = tour_city(tour)
    tour_datetime = tour_start_date_time(tour) 
    local_current_datetime = local_time_now(city) 
    tour_date + 3 >= local_current_datetime
  end

  def tour_start_date_time(tour)
    d = tour.tour_date
    t = tour.start_time
    dt = DateTime.new(d.year, d.month, d.day, t.hour, t.min, t.sec, t.zone)
  end

  def local_time_now(city)
    begin 
      timezone = get_timezone(city) # Create instance form Timezone::Zone
      timezone.time(Time.now)
    rescue => e
      logger.tagged("Google API Error") { logger.fatal(e) }   
      Time.now
    end
  end

  def tour_city(tour)
    tour.city
  end

  def get_geolocation(city)
    city_geolocation_hash(city)
  end

  def get_timezone(city)
    geolocation_array = get_geolocation(city)
    begin
      Timezone::Zone.new :latlon => geolocation_array
    rescue => e
      logger.tagged("Google API Error") { logger.fatal(e) }
      nil 
    end
  end

  def time_now_local_time(city)
    get_timezone(city)
  end

  def print_timezone(tour)
    city = tour_city(tour)
    time_now_local_time(city).try(:active_support_time_zone)
  end
  
  #tour/show page 
  def destinations_geolocation_array(tour)
    result = tour.destinations.map{|destination| [destination.name, destination.geolocation.latitude.to_f, destination.geolocation.longitude.to_f] }
    result
  end

  def city_geolocation_hash(city)
    geolocation_array = [city.geolocation.latitude.to_f, city.geolocation.longitude.to_f]
  end

  def destinations_geolocation_array_from_city(city)
    geolocation_array = []
    city.tours.each do |tour|
      tour.destinations.each do |destination|
        geolocation_array << [destination.name, destination.geolocation.latitude.to_f, destination.geolocation.longitude.to_f, tour.id, tour.title]
      end
    end
    geolocation_array
  end

end
