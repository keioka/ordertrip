module BaseHelper

  def a_b
    rand(2)
  end

  def print_google_analytics_code(env)
    current_user ? "ga('create', '#{get_uaid(env)}', { 'userId': '#{current_user.id}' });" : "ga('create', '#{get_uaid(env)}', 'auto');"
  end

  def get_uaid(env)
    if env == "development"
      'UA-67492497-1'
    elsif env == "staging"
      'UA-67492497-2'
    elsif env == "production"
      'UA-67492497-3'
    end
  end
end
