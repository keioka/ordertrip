module SessionsHelper

  def current_user=(user)
    @current_user = user
  end

  def current_user 
    if session[:user_id]
      @current_user ||= User.find(session[:user_id])
    end
    @current_user
  end 

  def get_session_key
    page_view_cookie = cookies[:_crvn_pv]
    JSON.parse(page_view_cookie)
  end

  def sign_in(user, login_method)
    session[:user_id] = user.id
    session_key = get_session_key["_sk"]
    # begin 
    #   HTTParty.post("http://localhost:8080/login_histories?login_history[user_id]=#{user.id}&login_history[login_method]=#{login_method}&login_history[session_key]=#{session_key}")
    # rescue Errno::ECONNREFUSED => e
    #   logger.error(e)
    # end
  end

  def sign_in?
    !current_user.nil?
  end

  def is_current_user?(id)
    session[:user_id] == id if session[:user_id]
  end

  def session_nil?
    session[:user_id].nil?
  end

end
