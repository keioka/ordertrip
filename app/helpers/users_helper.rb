module UsersHelper

  def city_name_from_user(user)
    "#{user.city.name}, #{user.city.country}" unless user.city.nil?
  end

  def user_print_member_since(user)
    datetime = user.member_since
    datetime.strftime("#{datetime.day.ordinalize} %b, %Y")
  end

  def get_user(id)
    @user = User.find(id) 
  rescue ActiveRecord::RecordNotFound => e
    logger.warn(e)
    nil
  end

  def make_string_languages(languages)
    languages.map{|lang| lang.name}.join(" ")
  end
  
  def user_tags_array(user)
    tags = user.tags.pluck(:title)
    tags.any? ? tags.compact : []
  end

  def user_languages_array(user)
    langs = user.languages.pluck(:name)
    langs.any? ? langs.compact : []
  end

  def show_first_name(user)
    user ? user.first_name : "No User"
  end

  def print_user_image(user, size=nil)
    
    if user == nil
      user_image_url = asset_path("no_user_image.jpg") 
    elsif user.image_urls.any?
      user_image_url = user.image_urls.first.url
    elsif user.uploaded_images.any?
      size = get_user_image_size(size)
      user_image_url = user.uploaded_images.first.image.url(size)
    else
      user_image_url = asset_path("no_user_image.jpg")
    end
    user_image_url

  end

  def get_user_image_size(size)
    if size == "small"
      :profile_sm
    elsif size == "large"
      :profile_lg
    end
  end

  # Verification

  def print_facebook_status(user)
    if user
      if user.verified_facebook?
        "iv-facebook"
      else
        "iv-facebook-false"
      end
    else
      "facebook_false"
    end
  end

  def print_phone_status(user)
    if user
      if user.verified_phone_number?
        "iv-phone"
      else
        "iv-phone-false"
      end
    else
      "iv-phone-false"
    end
  end

  def print_driver_license_status(user)
    if user
      if user.verified_driver_license?
        "iv-driver-license"
      else
        "iv-driver-license-false"
      end
    else
      "iv-driver-false"
    end
  end

  def print_criminal_record_status(user)
    if user
      if user.verified_sex_offender?
        return "iv-criminal-record"
      else
        return "iv-criminal-record-false"
      end
    else
      "iv-criminal-record-false"
    end
  end
end
