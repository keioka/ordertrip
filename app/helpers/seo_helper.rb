module SeoHelper

  def set_seo_elements(title=nil, city=nil, keywords=[])
    @city_for_seo = city
    @page_title = title
    @page_keywords = keywords
  end

  def page_title
    if @city_for_seo == nil
      @page_title ||= "No tilte - OrderTrip"
    else
      @page_title ? @page_title + " (" + @city_for_seo + ") - OrderTrip" : @city_for_seo + " - OrderTrip"
    end
  end 

  def page_description
    @city_for_seo == nil ? default_description : with_city_description(@city_for_seo)
  end

  def page_keywords
    @page_keywords == nil ? default_keywords : @page_keywords.join(" ") + default_keywords 
  end

  def default_description 
    "Private customized tour guided by locals. Post your ideal tour plan and travel with locals. Join OrderTrip community."
  end

  def with_city_description(city)
    "Private customized tour guided by locals in #{city}. Post your ideal tour plan and travel with locals. Join OrderTrip community."
  end

  def default_keywords
    "city guides local locals guide tour tours trip travel private customized customize"
  end

  def default_meta_tags
     {
      title: page_title,
      description: page_description,
      keywords:    page_keywords,
      separator:   "&mdash;".html_safe,
      fb: {
        app_id: 1650174651866023
      },
      og: {
        title:    page_title,
        type:     'website',
        keywords: [],
        description: page_description,
        url: request.original_url,
        image: {
          _: "#{asset_path('https://s3-us-west-1.amazonaws.com/ordertrip-assets/assets/images/OrderTrip-fb-og-image.jpg')}",
          width: 1200,
          height: 630,
        }
      },
      twitter: {
        card: "Find a local guide for your customized tour!",
        site: "@ot-ahoy",
        title: page_title,
        description: page_description,
        image: {
          _: "#{asset_path('https://s3-us-west-1.amazonaws.com/ordertrip-assets/assets/images/OrderTrip-twt-card-image.jpg')}",
          width:  435,
          height: 228,
        }
      }
    }
  end

  def default
   
  end

  def facebook
    set_meta_tags 
  end

  def twitter 
    set_meta_tags twitter: {
      card: "Find a local guide for your customized tour!",
      site: "@ot-ahoy",
      title: page_title,
      description: page_description,
      image: {
        _: "OrderTrip-twitter-card-image.jpg",
        width:  435,
        height: 228,
      }
    }
  end
end








