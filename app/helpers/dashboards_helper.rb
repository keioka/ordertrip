module DashboardsHelper
  def any_review?(user_review)
    user_review.try(:length) > 0
  end

  def city_name(city_id)
    begin
      city = City.find(city_id) 
      city.try(:name)
    rescue ActiveRecord::RecordNotFound => e
      logger.warn(e)
    end
  end
end
