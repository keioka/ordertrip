$(document).on('ready page:load', function (){
  $('.cancel-btn').on("click", function(e){
    e.preventDefault();
    var that = this;
  	swal({
  		title: "Cancel fee will occur",
  		text: "You cancel within 3 days before the tour date, cancel fee will not occur. ",
  		type: "warning",
  		showCancelButton: true,
  		confirmButtonColor: '#ED4A6D',
  		confirmButtonText: 'Yes, cancel this offer!',
  		cancelButtonText: "No!",
  		closeOnConfirm: false,
  		closeOnCancel: true
  	},
  	function(isConfirm){
      if (isConfirm){
        $.ajax({
          url: window.location.pathname + "/cancel" ,
          method: "get"
        }).success(function(){
           swal("Canceled!", "Please check your cancel fee", "success");
        }).error(function(){
           swal("Opps", "We couldn't delete it", "error");
        });
      } else    {
        swal("Cancelled", "Your offer is saved", "error");
      }
  	}.bind(that));
  });

  $('.cancel-btn-with-fee').on("click", function(e){
    e.preventDefault();
    var that = this;
    swal({
      title: "Cancel fee will occur",
      text: "You cancel within 3 days before the tour date, cancel fee \n (30% of tour fee) will be paid to local guide.",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#ED4A6D',
      confirmButtonText: 'Yes, cancel this offer!',
      cancelButtonText: "No!",
      closeOnConfirm: false,
      closeOnCancel: true
    },
    function(isConfirm){
      if (isConfirm){
        $.ajax({
          url: window.location.pathname + "/cancel" ,
          method: "get"
        }).success(function(){
           swal("Canceled!", "Please check your cancel fee", "success");
        }).error(function(){
           swal("Opps", "We couldn't delete it", "error");
        });
      } else    {
        swal("Cancelled", "Your offer is saved", "error");
      }
    }.bind(that));
  });

  $('#stripe-btn').on('click', function(event) {
    event.preventDefault();
    var $button = $(this),
        $form = $button.parents('form');
    var opts = $.extend({}, $button.data(), {
        token: function(result) {
          $form.append(
            $('<input>').attr({ 
              type: 'hidden', 
              name: 'stripeToken', 
              value: result.id
          })).submit();
        }
    });
    StripeCheckout.open(opts);
  });

  // var handler = StripeCheckout.configure({
  //   key: 'pk_test_5OkWt96cuFZGlYSsn6nKVMu2',
  //   image: '/img/documentation/checkout/marketplace.png',
  //   locale: 'auto',
  //   token: function(token) {
  //     var $input = $('<input type=hidden name=stripeToken />').val(token.id);
  //     $('form').append($input).submit();
  // });

  // $('#customButton').on('click', function(e) {
  //   // Open Checkout with further options
  //   handler.open({
  //     name: <%= @tour.title %>,
  //     description: 'Payment',
  //     amount: <%= @offer.price.to_i %>
  //   });
  //   e.preventDefault();
  // });

  // // Close Checkout on page navigation
  // $(window).on('popstate', function() {
  //   handler.close();
  // });

});
