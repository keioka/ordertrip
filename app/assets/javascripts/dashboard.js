$(document).on('ready page:load', function (){

  //https://github.com/bchanx/slidr
  (function(){
    //take length
    //imgSlider
    var imgSlider = {

      length: null,
      images: null,
      ctrlBox: null,
      activeIndex: 0,
      //Set elements
      setLength: function(){
        if (this.length == null){
          this.length = this.images.length;
        }
      },

      setImages: function(){
        if (this.images == null){
          this.images = $("img[data-is-role='img']");
        }
        this.hideImages();
        this.showImage(0);
      },

      setCtrl: function(){
        if (this.ctrlBox == null){
          this.ctrlBox = $("div[data-is-role='ctrl-box']")[0];
        }
      },

      onClickCtrl: function(){
      },

      init: function(){
        this.setImages();
        this.setCtrl();
        this.setLength();
        this.createElement();
        this.bindEvent();
      },

      createElement: function(){
        for (i = 0; i < this.length; i++){
          var circle;
          i == 0 ? circle = this.createCtrl("active") : circle = this.createCtrl("noactive");
          this.showCtrl(circle);
          // this.hideImages(i);
        }
      },

      hideImages: function(){
        this.images.map(function(idx, obj){
          obj.style.display = "none";
        });
      },

      showCtrl: function(circle){
        this.ctrlBox.appendChild(circle);
      },

      createCtrl: function(status){

        var ctrl = document.createElement("i");
        ctrl.setAttribute("data-is-role", "ctrl");
        if (status == "active"){
          ctrl.setAttribute("class", "fa fa-circle circle ctrl-active")
        } else {
          ctrl.setAttribute("class", "fa fa-circle circle")
        }
        return ctrl;
      },

      activate: function(ele){
        this.deactivate();
        $(ele).addClass('ctrl-active');
      },

      deactivate: function(){
        $("i[data-is-role='ctrl']").map(function(idx, obj){
          $(obj).removeClass('ctrl-active');
        });
      },

      bindEvent: function(){
        var that = this;
        $(document).on("click", "i[data-is-role='ctrl']", function(e){
          e.preventDefault();
          // Get which circle is clicked
          // this
          // show image which ctrl is clicked
          // active circle which is clicked
          var index = $("i[data-is-role='ctrl']").index(this);
          if(index != this.activeIndex){
            that.activate(this);
            that.hideImages();
            that.showImage(index);
            that.activeIndex = index;
          } 

        }).bind(that);
      },

      showImage: function(i){
        this.images[i].style.display = "";
      }
    }

    try { 
      imgSlider.init();
    } catch(err) {
      console.log(err);
    }
  }());
　
　(function(){
    if (window.location.pathname === "/dashboard"){
      $('.dashboard-nav')[0].addClass("dashboard-nav-active");
    } else if (window.location.pathname === "/dashboard/reviews"){
      $('.dashboard-nav')[1].addClass("dashboard-nav-active");
    } else if(window.location.pathname === "/dashboard/orders"){
      $('.dashboard-nav')[2].addClass("dashboard-nav-active");
    } else if(window.location.pathname === "/dashboard/offers"){
      $('.dashboard-nav')[3].addClass("dashboard-nav-active");
    } else if(window.location.pathname === "/dashboard/transaction"){
      $('.dashboard-nav')[4].addClass("dashboard-nav-active");
    }
  }());



  $('.delete-confirm').on("click", function(e){
    e.preventDefault();
    var that = this;
  	swal({
  		title: "Are you sure?",
  		text: "You will not be able to recover your order",
  		type: "warning",
  		showCancelButton: true,
  		confirmButtonColor: '#ED4A6D',
  		confirmButtonText: 'Yes, delete it!',
  		cancelButtonText: "No, cancel plx!",
  		closeOnConfirm: false,
  		closeOnCancel: true
  	},
  	function(isConfirm){
      if (isConfirm){
        $.ajax({
          url: $(that).attr("href"),
          method: "DELETE"
        }).success(function(){
           $(that).parent().parent().remove();
           swal("Deleted!", "Your imaginary file has been deleted!", "success");
        }).error(function(){
           swal("Opps", "We couldn't delete it", "error");
        });
      } else    {
        swal("Cancelled", "Your imaginary file is safe :)", "error");
      }
  	}.bind(that));
  });

  $('.user-delete-confirm').on("click", function(e){
    e.preventDefault();
    var that = this;
    swal({
      title: "Are you sure?",
      text: "You will not be able to recover your user data",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#ED4A6D',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: "No, cancel plx!",
      closeOnConfirm: false,
      closeOnCancel: true
    },
    function(isConfirm){
      debugger
      if (isConfirm){
        $.ajax({
          url: $(that).attr("href"),
          method: "DELETE"
        }).success(function(){
           swal("Deleted!", "Your imaginary file has been deleted!", "success");
           window.location = "/"
        }).error(function(){
           swal("Opps", "We couldn't delete it", "error");
        });
      } else    {
        swal("Cancelled", "Your imaginary file is safe :)", "error");
      }
    }.bind(that));
  });
});
