$(document).on('ready page:load', function (){
  // $('.signup').hide();
  // $('.login').hide();
  if (window.location.pathname == "/"){
  	$('.header').css({'position':'fixed'});
  	$(window).scroll(function(){
  	  var $top = $('body').scrollTop();
  	  if( $top > 0 ){
  	    $('.header').css('background-color', '#4bbefc');
  	  } else {
  	    $('.header').css('background-color', '');
      }
   	});
  } else {
    $('.header').css({'position':'absolute'});
  	$('.header .nav li a').css({color:'white'});
  }
  
  $('#lang-option').on('change', function(e){ 
    $value = this.value,
      path = window.location.pathname.replace(/en\/|ja\//m, "");
    if ($value == "English"){ 
      window.location = "/en" + path  
    } else {
      window.location = "/ja" + path
    } 
  });

});
