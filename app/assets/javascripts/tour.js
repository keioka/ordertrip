$(document).on('ready page:load', function (){

 $(".edit_user").validationEngine();
 $(".signup-form").validationEngine();
 $(".login-form").validationEngine();
 $(".offer-form").validationEngine();

 
  //*****************************************************
  //************           tour        ******************
  //*****************************************************

  // ########### refresh ############# -> tour/new
  // if refresh is clicked, run google map serch function

  $('.refresh').on('click', function(e){
    e.preventDefault();
    search();
  });


  // ########### refresh ############# -> tour/new
  // if refresh is clicked, run google map serch function

  (function(){
	  var $budget = $('.budget').val();
	  $('.budget-span').html($budget);
	  $('.budget').on("change mousemove", function(event){
	    $('.budget-span').text( $(this).val() );
	  });
	}());
	

  // ########### tour show ############# -> tour/show & city/show
  // if it is not /tours/new page and window.position top is over 480px, make right column is static.

  if (window.location.pathname != "/tours/new" && window.outerWidth > 480){
  // download complicated script
  // swap in full-source images for low-source ones
    (function(){
      var $mapPostion = $('#right').position().top;
		  
      window.onscroll = function(){
			  fixed($mapPostion)
		  }.bind($mapPostion);

		  function fixed(mapPostion){
			  var $top = $('body').scrollTop();
			  if ($top > mapPostion) {
			    $('#right').css({
			  	  'position': 'fixed',
			  	  'right': '0',
			  	  'top': '0'
			    });
			  } else if ($top < mapPostion){
			    $('#right').css({
			  	  'position': 'static'
			    });
			  }
		  };
    }());
	} 


  // ########### submit-tour-new ############# -> tour/new
  // if refresh is clicked, run google map serch function

  $('.submit-tour-new').on("click", function(e){
    e.preventDefault();
    var $validate = formValidation();
    if ($validate == false){
      swal({
  	    title: "Invalid",
  	    text: "You have empty input",
  	    type: "warning",
  	    confirmButtonColor: '#ED4A6D',
  	    confirmButtonText: 'OK',
  	    closeOnConfirm: false,
  	    closeOnCancel: false
      })
    } else {
      var $data = {
    	  "destinations":[]
      };
      var $tour = {};
    	$('#form-destination-info').serializeArray().map(function(x){$data["destinations"].push(x.value)}); 
      $("#form-city-info").serializeArray().map(function(x){$data[x.name] = x.value}); 
      $("#form-tour-info").serializeArray().map(function(x){$data[x.name] = x.value}); 
      $.ajax({
    	  url: "/tours",
    	  type: "POST",
    	  beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
    	  data: $data
      }).success(function(response){
        window.location.href = "/dashboard/orders"
      }).error(function(response){
        var errorMessage = JSON.parse(response.responseText)
        swal({
        title: "Error",
        text: errorMessage.message,
        type: "warning",
        confirmButtonColor: '#ED4A6D',
        confirmButtonText: 'OK',
        closeOnConfirm: false,
        closeOnCancel: false
      })
      });
    }
  });

  // ########### formValidation ############# -> tour/new
  // if refresh is clicked, run google map serch function

  function formValidation(){
    return formTourInfoValidation() == true && formCityInfoValidation() == true ? true : false 
  }


  // ########### formTourInfoValidation ############# -> tour/new
  // if refresh is clicked, run google map serch function

  function formTourInfoValidation(){
    var $validate = true
    $("#form-tour-info input").each(function(){
    	if (typeof this.value == undefined || this.value == ""){
    	  $validate = false;
    	  return $validate;
    	}
    })
    return $validate;
  }

  
  // ########### refresh ############# -> tour/new
  // if refresh is clicked, run google map serch function

  function formCityInfoValidation(){
  	var $cityInfo = $("#autocomplete")[0	].value;
  	return typeof $cityInfo == undefined || $cityInfo == "" ? false : true;
  }


});
