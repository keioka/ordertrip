// .popup-black
// #signup
// #login
// #signup-top-btn

$(document).on('ready page:load', function (){
  // $('.signup').hide();
  // $('.login').hide();
  
  //*****************************************************
  //************      User Form Edit     ****************
  //*****************************************************

  (function(){
    var userEditForm = {
      forms: null,
      length: null,
      navBox: null,
      activeIndex: 0,
      //Set elements
      setLength: function(){
      	if (this.length == null){
          this.length = this.forms.length;
        }
      },
  
      setForms: function(){
        if (this.forms == null){
          this.forms = $(".box-form-profile");
        }
        this.hideForms();
        this.showForm(0);
      },
  
      setNav: function(){
        if (this.navBox == null){
          this.navBox = $('.nav-user-form');
        }
      },
  
      onClickCtrl: function(){
      },
  
      init: function(){
        this.setForms();
        this.setNav();
        this.setLength();
        this.createElement();
        this.bindEvent();
        this.bindEventNextBtn();
      },
  
      createElement: function(){
      },
  
      showForm: function(i){
        this.forms[i].classList.add("slideUp");
        this.forms[i].style.display = "";
      },
  
      hideForms: function(){
        this.forms.map(function(idx, obj){
          obj.style.display = "none";
        });
      },
  
      showCtrl: function(circle){
      },
  
      activate: function(ele){
        this.deactivate();
        $(ele).addClass('ctrl-active');
      },
  
      deactivate: function(){
        $(this.navBox).map(function(idx, obj){
          $(obj).removeClass('ctrl-active');
        });
      },
  
      bindEvent: function(){
        var that = this;
        $(document).on("click", '.nav-user-form', function(e){
          e.preventDefault();
          // Get which circle is clicked
          // this
          // show image which ctrl is clicked
          // active circle which is clicked
          var $index = $('.nav-user-form').index(this);
          console.log($index);
  
          if($index != this.activeIndex){
            that.activate(this);
            that.hideForms();
            that.showForm($index);
            that.activeIndex = $index;
          } 
        }).bind(that);
      },
      bindEventNextBtn: function(){
        var that = this;
        $(document).on("click", '.user-edit-next', function(e){
          e.preventDefault();
          // Get which circle is clicked
          // this
          // show image which ctrl is clicked
          // active circle which is clicked
          var $index = that.activeIndex;
          console.log($index);
          var incrementedIndex = $index + 1
          if (incrementedIndex > 2){ incrementedIndex = 0; }
          if($index != this.activeIndex){
            var activateNavEle = that.navBox[incrementedIndex]
            that.activate(activateNavEle);
            that.hideForms();
            that.showForm(incrementedIndex);
            that.activeIndex = incrementedIndex;
          } 
        }).bind(that);
      }
    }
  
    try { 
      window.userEditForm = userEditForm;
      userEditForm.init();
    } catch(err) {
      console.log(err);
    }
  
  }());

});
