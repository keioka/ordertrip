$(document).on('ready page:load', function (){
  $('#city-search').on("click", function(event){
  	event.preventDefault();
  });

  $('.form-request-reset-password').on("submit", function(event){
    event.preventDefault();
    $.ajax({
    	method: "POST",
    	url: "/sessions/send_reset_token",
    	data: $(this).serialize()
    }).success(function(response){
      console.log(response);
      $('#box-result').text(" ");
      $('#box-result').text(response.message);
    }).error(function(response){
      console.log(response);
    });
  });

  $('.form-set-reset-password').on("submit", function(event){
    event.preventDefault();
    $.ajax({
      method: "POST",
      url: "/sessions/reset_password",
      data: $(this).serialize()
    }).success(function(response){
      console.log(response);
      if (response.status == 200){
        event.preventDefault();
        swal({
          title: "It is done!",
          text: response.message,
          showCancelButton: false,
          confirmButtonColor: '#ED4A6D',
          confirmButtonText: 'Yes, sure!',
          closeOnConfirm: true,
          closeOnCancel: true
        },function(isConfirm){
          if (isConfirm){
            window.location.href = "/"
          }
        });
      } else {
        $('#pr-alert').text(response.message);
      }
    }.bind(this)).error(function(response){
      console.log(response);
    });
  });
});
