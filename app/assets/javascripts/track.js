$(document).on('ready page:change', function (){
  // (function() { sendPageView(); }());
});
//   sendPageView();
//   alert("message");
// });

function getCookie(c_name){

 var i,x,y,ARRcookies=document.cookie.split(";");

 for (i=0;i<ARRcookies.length;i++)
 {
     x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
     y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
     x=x.replace(/^\s+|\s+$/g,"");
     if (x==c_name)
     {
         return unescape(y);
     }
  }
}

function createPageViewData(cookieObj){
  var object = JSON.parse(cookieObj);
  var session_key = object["_sk"]
  var session_count = object["_ct"]
  var referral = getReferral();
  var queryParams = getParams();
  var userAgent = getUserAgent();
  var dateTime = new Date;
  var path_name = getCurrentPath();
  var request = {
    "session_key": session_key,
    "session_count": session_count,
    "device": userAgent,
    "path": {
      path: path_name,  
      time : dateTime
    },
    "referral": {
      "url": referral,
      "keyword": ""
    },
    "campaign": queryParams
  }
  console.log(request)
  return request
}

function getCurrentPath(){
  var path = window.location.pathname;
  return path;
}

function getReferral(){
  var referrer = document.referrer
  return checkReferral(referrer);
}

function checkReferral(ref){
  var search_string = "/localhost:3000|ordertrip.com/g"
  var search_result = ref.search(search_string);
  return (search_result === -1 || search_result === 0) ? ref : undefined;
};

function getParams(){
  var urlParams;
  (window.onpopstate = function () {
    var match,
      pl     = /\+/g,  // Regex for replacing addition symbol with a space
      search = /([^&=]+)=?([^&]*)/g,
      decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
      query  = window.location.search.substring(1);
  
    urlParams = {};
    while (match = search.exec(query))
      urlParams[decode(match[1])] = decode(match[2]);
  })();
  return urlParams;
}

function getUserAgent(){
  var agent = navigator.userAgent
  return agent;
}

function sendPageView(){
  cookieObj = getCookie("_crvn_pv");
  // {"_sk":"_Uls3IzVRsM","_ct":2,"_stm":"2015-12-02T13:14:04.423-08:00"}
  var data = createPageViewData(cookieObj);
  send(data);
}

function send(data){
  $.ajax({
    dataType: "json",
    url: "http://localhost:8080/page_views",
    data: {"page_view":data},
    method: "POST" 
  }).success(function(response){
    console.log(response)
  }).error(function(response){
    console.log(response)
  });
}
