$(document).on('ready page:load', function (){
  if(window.location.pathname.match(/\/users\/\d+\/edit/) != null){
    (function(){
    	var value = $('#user_info_birthday').val();
    	var array = value.split("-");
      $('#date_day').val(parseInt(array[2]));
      $('#date_month').val(parseInt(array[1]));
      $('#date_year').val(parseInt(array[0]));
    }());

    $('select').on("change", function(e){
      $(this).val();
      var $day = $('#date_day').val();
      var $month = $('#date_month').val();
      var $year = $('#date_year').val();
      var value = $year + "-" + $month + "-" + $day;
      $('#user_info_birthday').val(value);
    });
  }

  if(window.location.pathname.match(/\/tours\/new/) != null){
    (function(){
      var value = $('#tour_date').val();
      var array = value.split("-");
      $('#date_day').val(parseInt(array[2]));
      $('#date_month').val(parseInt(array[1]));
      $('#date_year').val(parseInt(array[0]));
    }());

    $('select').on("change", function(e){
      $(this).val();
      var $day = $('#date_day').val();
      var $month = $('#date_month').val();
      if($month < 10){$month = "0" + $month}
      var $year = $('#date_year').val();
      var value = $year + "-" + $month + "-" + $day;
      $('#tour_date').val(value);
    });
  }
});