// .popup-black
// #signup
// #login
// #signup-top-btn

$(document).on('ready page:load', function (){
  // $('.signup').hide();
  // $('.login').hide();
  
  //*****************************************************
  //************      Header Start     ******************
  //*****************************************************


  if (window.location.pathname === "/"){
  	$('.header').css({'position':'fixed'});

  	$(window).scroll(function(){
  	  var $top = $('body').scrollTop();
  	  var $startpoint = $('.container').position().top - 50;
  	  if($top > $startpoint){
  	    $('.header').css('background-color', '#4bbefc');
  	  } else {
  	    $('.header').css('background-color', '');
      }
   	});

  } else {
    $('.header').css('background-color', '');
  }
  


  //*****************************************************
  //************      Header End     ********************
  //*****************************************************



  // ########### popup-black ############# 
  // if black background is clicked hide everthing poped up

  $('.popup-black').on("click", function(event){
    event.preventDefault();
    $('.popup-black').hide();
    $('.signup').hide();
    $('.login').hide();
    $('.payment-box').hide();
    $('body').css({
      'position':'static',
      'top': ''
    });
  });


  // ########## signup ######### ->  Header
  // if click signup button, popup black and the form are poped up

  $('.signup-btn').on("click", function(event){
    event.preventDefault();
    $('body').scrollTop(0);
    $('.popup-black').show();
    $('.signup').show();
    $('body').css({
      'position':'fixed',
      'top': -top
    });
    $('.popup-black').css({
      'position':'fixed'
    });
  });


  // #### login #### -> Header
  // if click login button, popup black and the form are poped up

  $('.login-btn').on("click", function(event){
    event.preventDefault();
    $('body').scrollTop(0);
    $('.popup-black').show();
    $('.login').show();
    $('body').css({
      'position':'fixed',
      'top': -top
    });
    $('.popup-black').css({
      'position':'fixed'
    });
  });


  // #### signup-top-btn #### -> city/index
  // if click login button, popup black and the form are poped up

  $('#signup-top-btn').on("click", function(event){
    event.preventDefault();
    $('body').scrollTop(0);
    $('.popup-black').show();
    $('.signup').show();
    $('body').css({
      'position':'fixed',
      'top': -top
    });
    $('.popup-black').css({
      'position':'fixed'
    });
  });


  // ############ .flash ############ -> application.html.erb
  // if click login button, popup black and the form are poped up

  $('.flash').bind('click', function(){
  	$(this).hide();
  }).bind(this);


  // ############ .offer ############ -> application.html.erb
  // if click login button, popup black and the form are poped up

  $('.offer').on("click", function(event){
  	event.preventDefault();
  	$('.offer-box').show();
  	// var offer = $('.offer');
  	// offer.text("Close box").removeClass("btn-x2").addClass("btn-gray offer-active");
  });


  // ############ .offer-close ############ -> application.html.erb
  // if click login button, popup black and the form are poped up

  $('.offer-close').on("click", function(event){
  	event.preventDefault();
  	$('.offer-box').hide();
  })

  // $(document).on("click", ".offer-active", function(event){
  // 	event.preventDefault();
  // 	$('.offer-box').hide();
  // 	var offer = $('.offer');
  // 	offer.removeClass("btn-gray offer-active").addClass("btn-x2");
  // });

  // #### .accept #### -> offer/show
  // if click login button, popup black and the form are poped up

  $('.accept').on("click", function(event){
  	event.preventDefault();
  	$('body').css({
      'position':'fixed',
      'top': -top
    });
    $('.popup-black').css({
      'position':'fixed'
    });
  	$('.payment-box').show();
  	$('.popup-black').show();
  });

  //*****************************************************
  //************     Pop up end    ********************
  //*****************************************************


});
