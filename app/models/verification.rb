class Verification < ActiveRecord::Base
  belongs_to :user
  
  def verify_block_score(params)
    # person = BlockScore::Person.create(
    #   birth_day: '23',
    #   birth_month: '8',
    #   birth_year: '1980',
    #   document_type: 'ssn',
    #   document_value: '0000',
    #   name_first: 'John',
    #   name_middle: 'Pearce',
    #   name_last: 'Doe',
    #   address_street1: '1 Infinite Loop',
    #   address_street2: 'Apt 6',
    #   address_city: 'Cupertino',
    #   address_subdivision: 'CA',
    #   address_postal_code: '95014',
    #   address_country_code: 'US'
    # )
    user = self.user
    first_name = user.first_name
    last_name = user.last_name
    birth_day_month = user.birthday.month
    birth_day_day = user.birthday.day
    birth_day_year = user.birthday.year
    passport_number = params[:passport_number] if params[:passport_number]
    ssn_last_four_digits = params[:ssn_last_four_digits] if params[:ssn_last_four_digits]
    BlockScore.api_key = 'sk_test_f3fa8fea7f961745f16f69c401d252e9'
    person = BlockScore::Person.create(
      birth_day: birth_day_day,
      birth_month: birth_day_month,
      birth_year: birth_day_year,
      document_type: 'ssn',
      document_value: ssn_last_four_digits,
      passport: passport_number,
      name_first: first_name,
      name_last: last_name,
      address_street1: '1 Infinite Loop',
      address_street2: 'Apt 6',
      address_city: 'Cupertino',
      address_subdivision: 'CA',
      address_postal_code: '95014',
      address_country_code: 'US'
    )
    p person
    if person.status == "valid"
      self.ssn = true
    else 
      self.ssn = false
    end
    self.save
  end
end
