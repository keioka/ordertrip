module TagConcern
  extend ActiveSupport::Concern

  def add_tag_to_user(tag_obj)
    current_user.tags << tag_obj
  end

  # def create_tag_from_array(hash)
  #   array = hash["tags"]
  #   return nil if array.nil?
  #   # tags = []
  #   array.each do |tag|
  #     title_id = tag.split(" ").join("").downcase
  #     tag_obj = Tag.find_by(title_id: title_id)
  #     if tag_obj.nil?
  #       tag_obj = Tag.create(title: tag)
  #     end
  #     current_user.tags << tag_obj
  #   end    
  # # User.find_each do |user|
  # #   NewsMailer.weekly(user).deliver_now
  # # end
  # end

# included do
#   has_many :taggings, as: :taggable, dependent: :destroy
#   has_many :tags, through: :taggings
# end

# def tag_names
#  tags.map(&:name)
# end
end
