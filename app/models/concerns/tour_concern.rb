module TourConcern
  extend ActiveSupport::Concern

  # def create_city(params)
  #   city_string = params["name"]
  #   google_place_id = params["google_place_id"]
  #   if city_string.include?(",")
  #     array = city_string.split(",")
  #     city = array[0]
  #     country = array[-1]
  #     city = City.find_or_create_by(name: city, country: country, google_place_id: google_place_id)
  #   else
  #     city = City.find_or_create_by(name: params.name)
  #   end
  #   city_with_geolocation = create_geolocation(city, params)
  #   return city_with_geolocation
  # end

  def create_geolocation(city, params)
    city.create_geolocation(latitude: params["latitude"], longitude: params["longitude"])
    return city
  end

  def tour_destinations(tour, params)
    params["destinations"].each do |google_place_id|
      destination = Destination.find_by(provider: "Google",provider_place_id: google_place_id)
      if destination 
        tour.destinations << Destination.find_by(provider: "Google",provider_place_id: google_place_id)
      end
    end
    tour.save
  end

  def get_geolocation(city)
    geolocation = city.geolocation
    [geolocation.latitude, geolocation.longitude]
  end

# included do
#   has_many :taggings, as: :taggable, dependent: :destroy
#   has_many :tags, through: :taggings 
# end

# def tag_names
#  tags.map(&:name)
# end
end
