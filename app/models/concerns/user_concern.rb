module UserConcern
  extend ActiveSupport::Concern
  
  def logged_in?
    !session[:user_id].nil? 
  end

  def validate_user(user)
    if user
      if user.first_name.nil?
        return "First Name"
      elsif user.last_name.nil?
        return "Last Name"
      elsif user.birthday.nil?
        return "Birthday"
      elsif user.city.nil?
        return "City"
      else
        true
      end       
    end
  end 
# included do
#   has_many :taggings, as: :taggable, dependent: :destroy
#   has_many :tags, through: :taggings 
# end

# def tag_names
#  tags.map(&:name)
# end
end
