class ToursDestination < ActiveRecord::Base
  belongs_to :tour
  belongs_to :destination
  validates :tour_id, uniqueness: { scope: :destination_id }
end
