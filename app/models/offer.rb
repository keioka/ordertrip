class Offer < ActiveRecord::Base
  has_many :messages, :dependent => :destroy
  has_one :charge, :dependent => :destroy
  has_many :reviews
  belongs_to :tour
  belongs_to :guide, :class_name =>'User', :foreign_key => 'guide_id'
  validates :tour_id, uniqueness: { scope: :guide_id }
  validates :price, :message, presence: true

  # delegate :sender_name, to: :tour, allow_nil: true
  # delegate :receiver_name, to: :tour, allow_nil: true
  delegate :guide_first_name, to: :guide, allow_nil: true
  delegate :guide_last_name, to: :guide, allow_nil: true
  delegate :tour_description, to: :tour, allow_nil: true
  
  def parent_tour
    self.tour
  end
  
  # ============= Tour ==========================

  def tour_title
    parent_tour.try(:title) 
  end

  def tour_id
    parent_tour.try(:id)
  end

  def tour_description
    parent_tour.try(:description)
  end
  
  # ============= Gudie =========================

  def guide_first_name
    guide.try(:first_name)
  end

  def guide_last_name
    guide.try(:last_name)
  end
  
  

  def accepted!(guide_id)
    return nil if self.nil?
    begin 
      self.update_attributes( accepted: true, accepted_at: DateTime.now )
      self.tour.update_attributes( guide_id: guide_id )
    rescue => e
      logger.warn(e.messages)
    ensure
      self
    end
  end

  def cancel!
    begin
      self.update_attributes(canceled: true, canceled_at: Time.now, canceled_at_local_time: self.local_time)
    rescue => e
      logger.warn(e.class, e.messages, e.backtrace)
    ensure
      self
    end
  end

  def local_time
    timezone.time Time.now
  end

  def timezone
    timezone = Timezone::Zone.new :latlon => self.get_geolocation
  end

  def get_geolocation
    tour = get_belonged_tour
    [tour.latitude, tour.longitude]
  end

  def get_belonged_tour
    self.tour.city.geolocation
  end

  # =========== Stripe fee =========================

  def stripe_fee
    (self.price * 0.03).round
  end
  
  # =========== Reviews =========================

  def review_post(review, reviewer, reviewee)
    self.reviews.create(body: review, reviewer_id: reviewer, reviewee_id: reviewee)
  end
end
