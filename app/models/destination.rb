class Destination < ActiveRecord::Base
  has_one :geolocation, as: :geolocation
  has_many :image_urls, as: :image_url
  has_many :google_photo_references

  def destination_has_any_photos?(destination)
    self.provider == "Google" ? self.google_photo_references.any? : self.image_urls.any?
  end

  def self.create_google_destination(places)
    response = []
    places.each do |place|
      place_id = place["place_id"]
      destination = Destination.find_or_initialize_by(provider_place_id: place_id, provider: "Google")
      if destination.new_record?

         place_reference = place["reference"]
         place_address = place["address"]
         place_name = place["name"]
         place_rating = place["rating"]
         place_position = place["position"]
      
         destination.update_attributes(name: place_name ,address: place_address, rating: place_rating)
         destination.create_geolocation(latitude: place_position["lat"], longitude: place_position["lng"])
         destination.save

         place_detail_request_url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + place_id + "&key=AIzaSyAfEZc3f_CMA9cR1TFh-dIKtUYu_fRo44w"  
    
         place_detail_response = HTTParty.get(place_detail_request_url)
         
         #Get google photo reference
         begin
           details = place_detail_response["result"]["photos"] unless place_detail_response.nil?
         rescue
           details = nil
         end

         reference_array = []

         unless details.nil?
           details.each do |detail|
             begin
               destination.google_photo_references.create(reference: detail["photo_reference"])
             rescue => e
               logger.warn "Unable to create reference, will ignore: #{place_name}" 
             end
             reference_array << detail["photo_reference"]
           end
         end
        # Return photo references
        response << reference_array
      else
        response << destination.google_photo_references.pluck(:reference)
      end
    end
    return response
  end
end
