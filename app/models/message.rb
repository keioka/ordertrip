class Message < ActiveRecord::Base
  belongs_to :offer
  belongs_to :sender, :class_name=>'User', :foreign_key=>'sender_id'
  belongs_to :receiver, :class_name=>'User', :foreign_key=>'receiver_id'

  delegate :sender_name, to: :sender, allow_nil: true
  delegate :receiver_name, to: :receiver, allow_nil: true
  # delegate :offer_canceled, to: :offer, allow_nil: true
  def sender_first_name
    sender.try(:first_name)
  end

  def sender_last_name
    sender.try(:last_name)
  end

  def receiver_name
    receiver.try(:first_name)
  end

end
