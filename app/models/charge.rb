class Charge < ActiveRecord::Base
  before_create :subscribtion_stripe_fee
  belongs_to :offer
  belongs_to :sender, :class_name =>'User', :foreign_key => 'sender_id'
  belongs_to :receiver, :class_name =>'User', :foreign_key => 'receiver_id'
  validates :offer_id, :sender_id, :receiver_id, presence: true
  validates :offer_id, uniqueness: { scope: :sender_id }

  delegate :sender_name, to: :sender, allow_nil: true
  delegate :receiver_name, to: :receiver, allow_nil: true
  delegate :offer_canceled, to: :offer, allow_nil: true

  def subscribtion_stripe_fee
    offer_price = self.offer.price
    stripe_fee = (offer_price * 0.03).round(2)
    self.stripe_fee = stripe_fee
    self.price = offer_price
  end
  
  # Delegate Methods
  def sender_name
    sender.try(:first_name)
  end

  def receiver_name
    receiver.try(:first_name)
  end

  def offer_canceled
    offer.try(:canceled)
  end

  def total_price
    self.price + self.stripe_fee
  end

  def offer_id
    self.offer.id
  end

  def tour_id
    self.offer.tour_id
  end
end
