class ImageUrl < ActiveRecord::Base
  belongs_to :image_url, polymorphic: true
end
