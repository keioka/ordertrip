class Tag < ActiveRecord::Base
  has_many :users_tag, :dependent => :destroy
  before_validation :create_tag_id
  before_save :capitalize
  validates :title_id, uniqueness: true
  
  private
    def capitalize 
      self.title = title.capitalize 
    end 

    def create_tag_id
      self.title_id = title.split(" ").join("").downcase if title
    end

    def self.find_or_create_from_array(attributes)
      if attributes.is_a?(Array)
        attributes.each do |attr|
          self.find_or_create_from_array(attr)
        end
      else
        tag_obj = self.find_by(title: attributes) || self.create(title: attributes)
        add_tag_to_user(tag_obj)
      end
    end
end
