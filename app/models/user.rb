class User < ActiveRecord::Base
  belongs_to :facebook_login, :dependent => :destroy
  belongs_to :email_login, :dependent => :destroy
  belongs_to :city
  
  has_one :verification, :dependent => :destroy
  has_one :email_notification, :dependent => :destroy

  #images
  has_many :image_urls, as: :image_url, :dependent => :destroy
  has_many :uploaded_images, :dependent => :destroy

  has_many :messages

  has_many :charged, class_name: "Charge", foreign_key: "sender_id"
  has_many :charging, class_name: "Charge", foreign_key: "receiver_id"

  # has_many :relationships, foreign_key: "follower_id", dependent: :destroy
  # has_many :reverse_relationships, foreign_key: "followed_id", class_name: "Relationship", dependent: :destroy

  has_many :tours, foreign_key: "traveler_id", :dependent => :destroy
  has_many :offered, through: :tours
  has_many :offers, foreign_key: "guide_id"

  has_many :users_language, :dependent => :destroy
  has_many :languages, through: :users_language

  has_many :users_tag, :dependent => :destroy
  has_many :tags, through: :users_tag

  has_many :reviews, foreign_key: "reviewee_id"
  has_many :reviewers, class_name: "User", foreign_key: "reviewer_id", through: :reviews

  has_many :reviewing, foreign_key: "reviewer_id"

  # validates :first_name, :last_name, :username, :email, :phone_number, :birthday, presence: true
  validates :email, :phone_number, :facebook_login_id, :email_login_id, :username, uniqueness: true, allow_nil: true

  after_save :create_user_verification
  after_create :create_user_email_notification
  after_update :check_email_login

  def full_name
    self.first_name + " " + self.last_name
  end

  def first_name
    first_name = read_attribute(:first_name)
    !first_name.nil? ? first_name : ""
  end

  def last_name
    last_name = read_attribute(:last_name)
    !last_name.nil? ? last_name : ""
  end
  
  def work
    work = read_attribute(:work)
    !work.nil? ? work : ""
  end

  # return gender with string 
  def gender_s
    self.gender == 1 ? "Male" : "Female"
  end

  def age
    if self.birthday
      now = Time.now.utc.to_date
      now.year - self.birthday.year - (birthday.to_date.change(:year => now.year) > now ? 1 : 0)
    end
  end

  def member_since
    self.created_at 
  end

  def user_profile_quality
    # self.first_name == "" 
  end

  def score_first_name
    self.first_name != "" ? 5 : 0
  end

  def score_last_name
    self.last_name != "" ? 5 : 0
  end

  def score_work
    self.work != "" ? 10 : 0
  end

  def score_age
    self.age != nil || self.age != "" ? 10 : 0
  end

  def score_description
    self.description
  end

  def create_user_verification
    if !self.facebook_login.nil? 
      self.verification ?  self.verification.update_attributes(facebook:true) : self.create_verification(facebook: true)
    else 
      self.verification ?  self.verification : self.create_verification
    end
  end

  # ================= verification ==================

  def check_block_score(user_verification_params)
    self.verification.verify_block_score(user_verification_params)
  end

  def find_or_create_verification
    self.verification.nil? ? create_user_verification : self.verification
  end

  def verified_facebook?
    verification = find_or_create_verification
    verification.facebook 
  end

  def verified_phone_number?
    !self.phone_number.nil?
  end

  def verified_driver_license?
    verification = find_or_create_verification
    verification.driver_license
  end

  def verified_sex_offender?
    verification = find_or_create_verification
    verification.sex_offender
  end


  def is_facebook_user?
    !self.facebook_login.nil?
  end

  def is_new_user?
    self.email_login.is_new_user? if self.email_login #EmailLogin
  end

  def sent_welcome_mail!
    self.email_login.sent_welcome_mail!
  end

  def is_confirmed_email?
    self.email_login.confirmed_email? #EmailLogin
  end

  def get_email_confirm_token
    self.email_login.confirm_token if self.email_login
  end

  def check_email_login
    if self.email_login 
      email = self.get_email_login
      if self.email != email
        email_login = self.email_login 
        email_login.update_attributes!(email: self.email)
        email_login.email_changed!
      end
    end
  end

  def get_email_login
    self.email_login.email if self.email_login
  end
  
  # ========== Email Notification ====================

  # create email notification after creating user
  def create_user_email_notification
    self.create_email_notification
  end
  
  # check welcome_email is sent or not 
  def sent_welcome_email?
    self.email_notification.welcome_email
  end
  
  # 
  def reset_password
    generate_and_assing_reset_token
  end

  def get_reset_token
    self.email_login.reset_password_token
  end

  def generate_and_assing_reset_token
    self.email_login.generate_reset_token
  end

  # Create tag from params tag params. 
  # This is used inside user#edit
  def create_tag_from_array(hash)
    array = hash["tags"]
    return nil if array.nil?
    array.each do |tag|
      title_id = tag.split(" ").join("").downcase
      tag_obj = Tag.find_by(title_id: title_id)

      if tag_obj.nil?
        tag_obj = Tag.create(title: tag)
      end

      unless self.tags.map(&:title_id).include?(tag_obj.title_id)
        #also validated and have uniqueness in user model
        self.tags << tag_obj
      end
    end 
    # array.map{|ele| { title: ele }}
    # Tag.find_or_create_by(array)
  end

  def delete_tag(tag)
    self.tags
  end

  def create_user_language(hash)
    array = hash["languages"]
    return nil if array.nil?
    array.each do |language| 
      begin 
        self.languages << Language.find_by(name: language)
      rescue
        logger.error("Can't push languages")
      end
    end
  end

  def delete_lang(hash)
    self.languages
  end
end
