class EmailLogin < ActiveRecord::Base
  after_save :create_user

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i

  has_secure_password

  has_one :user

  validates :email, format: { with: VALID_EMAIL_REGEX , message: "Invalid email address" }, presence: true, uniqueness: true
  
  before_create :generate_confirm_token
  # Create user after create email login
  def create_user
    user = User.create(email: self.email, email_login_id: self.id, created_at: self.created_at)
  rescue => e
    self.destroy
    logger.fatal(e + " | User isn't created!!")
  end 
  
  # Called
  def email_changed!
    self.reset_confirmed_email
    self.send_email_confirm_email
  end

  def reset_confirmed_email
    self.confirmed = false
    self.generate_confirm_token
    self.save!
  end

  def is_new_user?
    self.sent_welcome_mail.nil?
  end

  def sent_welcome_mail!
    self.sent_welcome_mail = true
    self.save
  end

  def confirmed_email?
    self.confirmed == true
  end

  def generate_confirm_token
    self[:confirm_token] = SecureRandom.urlsafe_base64
  end

  def generate_reset_token
    self[:reset_password_token] = SecureRandom.urlsafe_base64
    self.save!
  end

  def self.done_reset_password(reset_password_token)
    EmailLogin.find_rptoken_assign_nil(reset_password_token)
  end

  def self.find_email_login_from_rptoken(reset_password_token)
    email_login = EmailLogin.find_by(reset_password_token: reset_password_token)   
  end
  
  def self.find_rptoken_assign_nil(reset_password_token)
    email_login =  EmailLogin.find_by(reset_password_token: reset_password_token)   
    email_login.update_attributes!(reset_password_token: nil)
  end

  def send_email_confirm_email
    UserMailer.confirm_email_address(self).deliver_now
    logger.info("Successfully sending confirm email");
  rescue => e
    logger.error(e);
  end
end
