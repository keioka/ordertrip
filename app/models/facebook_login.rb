class FacebookLogin < ActiveRecord::Base
  include ProfileConcern
  has_one :user
 
  def create_facebook_login(response)
    
    gender = gender_to_number(response.extra.raw_info.gender) || 0
    email = response.extra.raw_info.email || " "
    first_name = response.extra.raw_info.first_name || " "
    last_name = response.extra.raw_info.last_name || " "

    # Create user
    user = self.create_user(
        email: email,
        first_name: first_name,
        last_name: last_name,
        gender:  gender
    )

    user.save!

    # Get user's images and create
    
    image_url = response.info.image
    if image_url
      user.image_urls.create(url: image_url)
    end
    
    # Language (retrive from response.extra.raw_info.languages )
    
    languages = response.extra.raw_info.languages
    
    if !languages.nil?
      languages_array = languages.map(&:name) 
      languages_array.each do |language|
        language = Language.find_or_create_by(name: language)
        user.languages << language
      end
    end

    user.save!
    return user
  
  end

  def update_facebook_login_user(response)

    gender = gender_to_number(response.extra.raw_info.gender) || 0
    email = response.extra.raw_info.email || " "
    first_name = response.extra.raw_info.first_name || " "
    last_name = response.extra.raw_info.last_name || " "

    # Create user
    if self.user.nil?
      user = self.create_user(
        email: email,
        first_name: first_name,
        last_name: last_name,
        gender:  gender
      )
      user.save!
    else
      user = self.user.update_attributes(
        email: email,
        first_name: first_name,
        last_name: last_name,
        gender:  gender
      )
    end

    # Get user's images and create
    image_url = response.info.image
    if image_url
      user.image_urls.create(url: image_url)
    end
    
    # Language (retrive from response.extra.raw_info.languages )
    languages = response.extra.raw_info.languages
    
    if !languages.nil?
      languages_array = languages.map(&:name) 
      languages_array.each do |language|
        language = Language.find_or_create_by(name: language)
        user.languages << language
      end
    end

    user
  
  end
end
