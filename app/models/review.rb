class Review < ActiveRecord::Base
  # User who is reviewed 
  belongs_to :reviewer, class_name: :User

  # User who reviews other user
  belongs_to :reviewee, class_name: :User, :counter_cache => :reviews_count

  belongs_to :offer

  validates :reviewer_id, uniqueness: { scope: :offer_id }
  validates :reviewee_id, uniqueness: { scope: :offer_id }
  validates :body, presence: true
end
