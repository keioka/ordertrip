class UploadedImage < ActiveRecord::Base
  belongs_to :user
  has_attached_file :image,
                    :styles => {
                      :profile_lg => "147x147#",
                      :profile_sm => "46x46#",
                      :medium  => "400x400>",
                      :large => "800x800>"
                    },
                    s3_protocol: 'https',
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/s3.yml",
                    :path => "/:attachment/users/:id/:style.:extension"
  validates_attachment :image, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }
  do_not_validate_attachment_file_type :image
end
