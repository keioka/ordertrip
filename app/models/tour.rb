class Tour < ActiveRecord::Base
  include TourConcern
  before_create :set_tour_date_into_time

  has_many :tours_tags, :dependent => :destroy
  has_many :tags, through: :tours_tags

  has_many :tours_destinations, :dependent => :destroy
  has_many :destinations, :through => :tours_destinations

  has_many :offers, :dependent => :destroy

  belongs_to :city
  belongs_to :traveler, class_name: "User", foreign_key: :traveler_id

  validates :title, :description, :budget, :tour_date, :start_time,
    :end_time, :city_id, :traveler_id, presence: true
    
  validate :expiration_date_cannot_be_in_the_past
  
  #Validation
  def expiration_date_cannot_be_in_the_past
    if tour_date < Date.today
      errors.add(:tour_date, "can't be in the past")
    end
  end

  def tour_ended?
  end

  def accepted?
    !self.guide_id.nil?
  end

  def tour_date_dmy
    tour_date.strftime('%x')
  end

  def set_tour_date_into_time
    date = self.tour_date 
    year = date.year
    month = date.month
    day = date.day
    self.start_time = self.start_time.change(year: year, month: month, day: day, zone: get_time_zone)
    self.end_time = self.end_time.change(year: year, month: month, day: day, zone: get_time_zone)
  end

  def get_time_zone
    city = City.find(self.city_id)
    geolocation_array = get_geolocation(city) #[latitude, longitude]
    timezone = Timezone::Zone.new :latlon => geolocation_array
    timezone.active_support_time_zone
  end
end
