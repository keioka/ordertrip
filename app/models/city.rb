class City < ActiveRecord::Base
  extend CityConcern
  before_save :create_id
  has_many :tours
  has_many :users
  has_one :geolocation, as: :geolocation
  has_one :city_list
  validates :city_name_id, uniqueness: true

  def self.create_city_from_params(params)
    city_params_array = self.make_city_array(params)
    state = nil
    if city_params_array.length > 3
    #city_params_array => ["San Francisco", " CA", " United States"]
      city_name = city_params_array[0] 
      state = city_params_array[-2].scan(/[a-zA-Z]+/).join(" ") #delete space
      country = city_params_array[-1].scan(/[a-zA-Z]+/).join(" ") #delete first space
      city_name_id = self.generate_id(city_name, state, country)
      google_place_id = params["google_place_id"]
      city = self.find_or_initialize_by(city_name_id: city_name_id)


    elsif city_params_array.length == 3
      city_name = city_params_array[0] 
      state = city_params_array[1].scan(/[a-zA-Z]+/).join(" ") #delete space
      country = city_params_array[2].scan(/[a-zA-Z]+/).join(" ") #delete first space

      city_name_id = self.generate_id(city_name, state, country)
      google_place_id = params["google_place_id"]
      city = self.find_or_initialize_by(city_name_id: city_name_id)


    #city_params_array => ["Tokyo" , " Japan"]
    elsif city_params_array.length == 2
      city_name = city_params_array[0] 
      country = city_params_array[1].scan(/[a-zA-Z]+/).join(" ") #delete first space

      city_name_id = self.generate_id(city_name, state, country)
      google_place_id = params["google_place_id"]
      city = self.find_or_initialize_by(city_name_id: city_name_id)
    

    #city_params_array => ["Tokyo" , " Japan"]
    elsif 
      # if user has this city data skip 
      # Warning ( Select city from the list)
      logger.warn("Select city from the list")
    end

    if city.new_record?
      city.update_attributes(name: city_name, state: state, country: country, google_place_id: google_place_id)
      city.build_geolocation(latitude: params["latitude"], longitude: params["longitude"]) 
      city.save!
    end

    if city.geolocation.latitude == nil || city.geolocation.longitude == nil
      city.geolocation.update_attributes(latitude: params["latitude"], longitude: params["longitude"]) 
    end      
    # return city
    city
  end

  private

    def create_id
      name_s = name.split(" ").join("").downcase if name
      state_s = state.split(" ").join("").downcase if state
      country_s = country.scan(/[A-Z]/).join("").downcase
      state_s ? self.city_name_id = name_s + "_" + state_s + "_" + country_s :  self.city_name_id = name_s + "_" + country_s
    end

    def self.generate_id(city, state=nil, country)
      name_s = city.split(" ").join("").downcase
      state_s = state.downcase if state
      country_s = country.scan(/[A-Z]/).join("").downcase
      state_s ? name_s + "_" + state_s + "_" + country_s :  name_s + "_" + country_s
    end

    def self.make_city_array(params)
      city_parmas_array = params["name"].split(",")
    end
end
