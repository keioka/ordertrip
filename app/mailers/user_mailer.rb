class UserMailer < ApplicationMailer
  default from: 'ahoy@ordertrip.com'
  
  def registration_confirmation(user)
    @welcome_user = user
    unless user.is_confirmed_email?
      token = user.get_email_confirm_token 
      @confirm_token_url = "http://www.ordertrip.com/sessions/confirm_email?ct=#{token}"
    end
    mail( 
      to: user.email, 
      subject: "Hi, #{user.first_name}! Welcome to OrderTrip [ OrderTrip ]", 
      template_path: 'user_mailer',
      template_name: 'registration_confirmation'
    )
  end

  def confirm_email_address(email_login)
    token = email_login.confirm_token
    if token
      @confirm_token_url = "http://www.ordertrip.com/sessions/confirm_email?ct=#{token}"
    end
    mail( 
      to: email_login.email, 
      subject: "Please confirm your email [ OrderTrip ]", 
      template_path: 'user_mailer',
      template_name: 'email_confirm'
    )
  end

  def reset_password_token(user, token)
    @reset_token_url =  "http://www.ordertrip.com" + "/sessions/set_new_password?rpt=#{token}"
    @reset_user = user
    mail( 
      to: user.email, 
      subject: "Hi, #{user.first_name}! Please Reset Your Password. [ OrderTrip ]", 
      template_path: 'user_mailer',
      template_name: 'reset_password_token'
    )
  end
end
