class OfferMailer < ApplicationMailer
  default from: 'ahoy@ordertrip.com'
  
  def send_messages_notification(offer_id, sender_id, receiver_id, url)
    @offer_title = Offer.find(offer_id).tour.title
    @sender = User.find(sender_id)
    @receiver = User.find(receiver_id)
    @url = "http://www.ordertrip.com" + url
    mail( 
      to: @receiver.email, 
      subject: "Hi, #{@receiver.first_name}! You got new meesage from #{@sender.first_name} [ OrderTrip ]", 
      template_path: 'offer_mailer',
      template_name: 'send_messages_notification'
    )
  end

  def send_offer_accepted_notification(offer_id, guide_id, traveler_id)
    # Offer accepted notification email. Guide wh
    @offer_title = Offer.find(offer_id).tour_title
    @sender = User.find(traveler_id)
    @receiver = User.find(guide_id)
    @url = "https://www.ordertrip.com/dashboard/transaction"
    mail( 
      to: @receiver.email, 
      subject: "Hi, #{@receiver.first_name}! Your offer is accepted. [ OrderTrip ]", 
      template_path: 'offer_mailer',
      template_name: 'offer_accepted'
    )
  end

  def send_confirmation_accepted_tour(offer_id, guide_id, traveler_id)
    # Offer Accpet Confirmation Email. Traveler gets email when they paid and accepted offer.
    @offer_title = Offer.find(offer_id).tour_title
    @receiver = User.find(traveler_id)
    @url = "https://www.ordertrip.com/dashboard/transaction" 
    mail( 
      to: @receiver.email, 
      subject: "Hi, #{@receiver.first_name}! Your payment is done. [ OrderTrip ]", 
      template_path: 'offer_mailer',
      template_name: 'offer_accept_confirmation'
    )
  end
end


















