class CampaignController < ApplicationController
  # utm_medium=social&utm_source=facebook.com&utm_campaign=functionfriday

  def instagram
    redirect_to "http://www.ordertrip.com/?utm_medium=social&utm_source=instagram" 
  end

  def facebook
    redirect_to "http://www.ordertrip.com/?utm_medium=social&utm_source=facebook" 
  end

  def twitter
    redirect_to "http://www.ordertrip.com/?utm_medium=social&utm_source=twitter"
  end

  def cp01
    redirect_to "http://www.ordertrip.com/?utm_medium=email&utm_campaign=cp01"
  end

  def cp02
  end
end
