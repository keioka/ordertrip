class ApplicationController < ActionController::Base
  
  include CityConcern
  include TagConcern
  include TourConcern
  include UserConcern
  include LanguageConcern
  include ProfileConcern  
  include OfferConcern
  include SessionsHelper
  include SeoHelper
  include TrackConcern
  
  include HttpAcceptLanguage::AutoLocale
  
  rescue_from Exception, with: :handle_500 unless Rails.env.development?
  rescue_from ActionController::RoutingError, with: :handle_404 unless Rails.env.development?
  rescue_from ActiveRecord::RecordInvalid, with: :handle_404 unless Rails.env.development?
  rescue_from ActiveRecord::RecordNotFound,   with: :handle_404 unless Rails.env.development?
  # rescue_from Exception, with: :handle_500 #unless Rails.env.development?
  # rescue_from ActionController::RoutingError, with: :handle_404 #unless Rails.env.development?
  # rescue_from ActiveRecord::RecordNotFound,   with: :handle_404 #unless Rails.env.development?

  helper_method :current_user
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  before_filter :page_view, :set_access_control_headers, :seo_variable_reset, :set_locale, :accept_language
  
  def show_ip
    # request.headers.sort.map { |k, v| logger.info "#{k}:#{v}" } 
    remote_ip = request.env["HTTP_X_FORWARDED_FOR"] || request.remote_ip
  end

  def get_user_id
    
  end
  
  def accept_language
    p I18n.locale
    user_preferred_lang = http_accept_language.user_preferred_languages
    if user_preferred_lang.include?("ja") && !request.path.split("/").include?("ja")
      I18n.locale = "ja"
    end
  end

  def page_view
    #set the key to browser
    if cookies.permanent[:_crvn_pv]
      crvn_pv = JSON.parse(cookies[:_crvn_pv])
      count = crvn_pv["_ct"]
      session_key = crvn_pv["_sk"]
      if new_session?(crvn_pv["_stm"])
        count += 1 
        cookies.permanent[:_crvn_pv] = { value: {_sk: session_key , _ct: count, _stm: Time.now}.to_json }
      end
      #post track server
    else
      session_key = get_random_session_key
      count = 1
      cookies.permanent[:_crvn_pv] = { value: {_sk: session_key , _ct: count, _stm: Time.now}.to_json }
    end

      
    #send the key to browser
    
    #If first session (after 30 min from last session)
    # create new session_key
    # send referal, campaign, 
    #if not 
    # send {path: time} and session key
    # inside tracker, find session key and push path hash 
    # if user logined 
    # show_ip
  end


  # =============== Locale ====================================
  
  def set_locale
    I18n.locale = params[:locale] if params[:locale].present?
  end

  def default_url_options(options={})
    { locale: I18n.locale }
  end
  
  
  # =============== User Email ================================
  def send_welcome_email(user) 
    UserMailer.registration_confirmation(user).deliver_now
    logger.info("Successfully sending welome email");
    user.sent_welcome_mail!
  rescue => e
    logger.error(e);
  end

  def send_email_confirm_email(email_login) 
    UserMailer.confirm_email_address(email_login).deliver_now
    logger.info("Successfully sent confirm email");
  rescue => e
    logger.error(e);
  end

  def send_reset_password_token(user, token) 
    UserMailer.reset_password_token(user, token).deliver_now
    logger.info("Successfully sent reset password token");
  rescue => e
    logger.error(e);
  end

  # =============== Offer Notification Email ================================
  
  def send_messages_notification(offer_id, sender_id, reciever_id, url)
    OfferMailer.send_messages_notification(offer_id, sender_id, reciever_id, url).deliver_now
    logger.info("Successfully sent message notification");
  rescue => e
    logger.error(e);
  end
  
  # task
  def send_got_offer_messages 
    OfferMailer.send_messages_notification(sender, reciever)
    logger.info("Successfully sent message notification");
  rescue => e
    logger.error(e);
  end

  def send_offer_accepted_mail(offer_id, guide_id, traveler_id)
    OfferMailer.send_offer_accepted_notification(offer_id, guide_id, traveler_id).deliver_now
    OfferMailer.send_confirmation_accepted_tour(offer_id, guide_id, traveler_id).deliver_now
    logger.info("Successfully sent accepted email notification and confirmation");
  rescue => e
    logger.error(e);
  end

  # =============== Error Handler ================================

  def handle_500(exception = nil)
    logger.warn "Rendering 500 with exception: #{exception.message}" if exception

    if request.xhr?
      # Ajax
      render json: { error: '500 error' }, status: 500
    else
      render template: '/errors/500', status: 500, layout: 'application', content_type: 'text/html'
    end
  end

  def handle_404(exception = nil)
    logger.warn "Rendering 404 with exception: #{exception.message}" if exception

    if request.xhr?
      # Ajax
      render json: { error: '404 error' }, status: 404
    else
      render template: '/errors/404', status: 404, content_type: 'text/html'
    end
  end
  
  def set_access_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Request-Method'] = 'GET, OPTIONS, HEAD'
    headers['Access-Control-Allow-Headers'] = 'x-requested-with,Content-Type, Authorization'
  end
  
  def seo_variable_reset 
    @city_for_seo = nil
    @page_title = nil
    @page_keywords = nil
  end

end
