class SessionsController < ApplicationController

  # POST sessions/create
  def create
    email_login = EmailLogin.find_by(email: params[:session][:email].downcase)
    if email_login && email_login.authenticate(params[:session][:password])
      user = User.find_by(email_login_id: email_login.id)
      sign_in(user, "Email")
      current_user
      unless validate_user(user)  == true
        redirect_to edit_user_path(user) 
      else
        redirect_to :back
      end
    else
      flash[:error] = "Please check your email & passowrd."
      redirect_to :back
    end
  end
 
  # DELETE sessions/logout
  def logout
    session["user_id"] = nil
    current_user = nil
    redirect_to root_path
  end

  # GET /request_new_password
  def request_new_password
    if current_user
      redirect_to dashboard_path
    end
  end
  
  # POST /send_reset_token
  # - Find email and send email to user if it is found
  def send_reset_token
    email = request_reset_password_params[:email]
    email_login = EmailLogin.find_by(email: email)
    # only Email find. should be facebook also

    if email_login
      user = email_login.user
      if user.nil?
      else
      end
      user.reset_password
      begin 
        send_reset_password_token(user, user.get_reset_token)
        render json: {status: 200, message: "We sent email. Please check it and reset password."}
      rescue => e
        logger.warn(e)
        render json: {status: 404, message: e.message}
      end
    else
      user = User.find_by(email: email)
      if !user.nil? && user.is_facebook_user?
        render json: {status: 404, message: "You seems logined with Facebook. Please login with Facebook and set password."}
      else
        render json: {status: 404, message: "We couldn't find your email address."}
      end
    end
  end
  
  # GET /set_new_password
  def set_new_password
    @reset_password_token = params[:rpt]
    if @reset_password_token 
      email_login = EmailLogin.find_by(reset_password_token: @reset_password_token)
      render "/errors/users/invalid_rptoken" if email_login.nil? 
    else
      render "/errors/users/invalid_rptoken" 
    end
  end
  
  # POST /reset_password
  def reset_password
    reset_password_token = rpt_params[:rpt]
    if reset_password_token 
      email_login = EmailLogin.find_email_login_from_rptoken(reset_password_token)
      if email_login
        begin
          email_login.update_attributes!(set_reset_password_params)
          EmailLogin.done_reset_password(reset_password_token)
          render json: {status: 200, message: "We have changed your password successfully. Please login with new password."}
        rescue ActiveRecord::RecordInvalid => e
          render json: {status: 404, message: e.message }
        end
      else
        render json: {status: 404, message: "We couldn't find your email address."}   
      end     
    end
    # "/users/reset_password"
  end

  def confirm_email
    confirm_token = params[:ct]
    email_login = EmailLogin.find_by(confirm_token: confirm_token)
    if email_login
      email_login.confirmed = true
      email_login.save!
      @result = "Your email is confirmed"
    else
      @result = "Your email isn't confirmed yet"
    end
  end

  private 
    def request_reset_password_params
      params.require(:request_reset_password).permit(:email)
    end

    def set_reset_password_params
      params.require(:set_reset_password).permit(:password, :password_confirmation)
    end

    def rpt_params
      params.require(:set_reset_password).permit(:rpt)
    end
end
