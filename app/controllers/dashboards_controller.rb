class DashboardsController < ApplicationController
   before_action  :sign_in_alert, :check_user_info

  def index
  end

  def review
  end

  def order
    @orders = Tour.where(traveler_id: current_user.id)
  end

  def offer
    @offers = Offer.includes(:tour).where(guide_id: current_user.id)
  end

  def transaction
  end

  private 
    def check_user_info
      if current_user.first_name.nil? || current_user.last_name.nil? || current_user.gender.nil? || current_user.city.nil? || current_user.city.nil? || current_user.birthday.nil? 
        redirect_to "/users/#{current_user.id}/edit"
      end
    end

    def require_login
      unless logged_in?
        flash[:error] = "You must be logged in to access this section"
        redirect_to :back # halts request cycle
      end
    end

    def sign_in_alert
      if session_nil?
       @page_title = "User Not Logined"
       render :template => "errors/partial/session/_nil", :locals => { type: "session_nil" }
      end
    end
end
