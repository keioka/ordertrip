class StaticPagesController < ApplicationController
  def mission
    render "layouts/static/mission"
  end

  def payment
    render "layouts/static/payment"
  end

  def team
    render "layouts/static/team"
  end

  def terms
    render "layouts/static/terms"
  end
end
