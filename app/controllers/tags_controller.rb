class TagsController < ApplicationController
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }
  def index
    @tags = Tag.all.pluck(:title)
    render json: @tags
  end
end
