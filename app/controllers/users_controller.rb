class UsersController < ApplicationController
  
  before_action :accessed_current_user?, only: [:edit, :update]

  def show
    @user = User.includes(:city, :languages, :reviews).find(params[:id])
  end

  def new
    @user = User.new
  end

  def create
    #Use after create
    email_login = EmailLogin.create(user_params)
    if email_login
      unless email_login.user.nil?
        flash[:success] = "Welcome to OrderTrip!"
        send_email_confirm_email(email_login)
        sign_in(email_login.user, "Email")
        current_user 
        redirect_to edit_user_path(email_login.user)
      else 
        flash[:error] = email_login.errors.full_messages
        redirect_to :back
      end
    else
      flash[:error] = email_login.errors.full_messages
      redirect_to :back
    end  
  end

  def edit
    @user ||= User.find(current_user.id)
  end

  def update

    user = User.find(current_user.id)

    #paper clip 
    if params[:images]
      params[:images].each { |image|
        user.uploaded_images.create(image: image)
      }
    end

    begin 
      city = City.create_city_from_params(city_params)
      begin
        user.update_attributes!(user_info_params.merge(city_id: city.id))
      rescue ActiveRecord::RecordInvalid => e
        flash[:error] = "Email has been already taken."
      end
      user.create_tag_from_array(tag_params)
      user.create_user_language(language_params)

      if validate_user(user) == true
        sign_in(user, "Email") if user.is_new_user?
        redirect_to "/dashboard"
      else 
        flash[:error] = "Check" + validate_user(user) 
        redirect_to edit_user_path(user)
      end

    rescue => e
      logger.error(e)
      if user.city
        begin
          user.update_attributes!(user_info_params)
        rescue ActiveRecord::RecordInvalid => e
          flash[:error] = e.message
        end
        user.create_tag_from_array(tag_params)
        user.create_user_language(language_params)
        if validate_user(user) == true
          sign_in(user, "Email") if user.is_new_user?
          redirect_to "/dashboard"
        else 
          flash[:error] = "Check" + validate_user(user) 
          redirect_to edit_user_path(user)
        end
      else
        begin
          user.update_attributes(user_info_params)
        rescue ActiveRecord::RecordInvalid => e
          flash[:error] = "Email has been already taken."
        end
        user.create_tag_from_array(tag_params)
        user.create_user_language(language_params)
        flash[:error] = "Please check you choose city form the list below the box" 
        redirect_to edit_user_path(@user)
      end 
    end
  end

  def destroy
    user = User.find(params[:id])
    user.destroy 
    session[:user_id] = nil
    render json: { status: "Success"}, status: 201
  rescue => e
    render json: { status: e }, status:404
  end

  private

    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation)
    end

    def user_info_params
      params.require(:user_info).permit(:email, :first_name, :last_name, :username, :gender, :description, :birthday, :city, :work)
    end

    def user_verification_params
      params.require(:user_verification).permit(:passport_number, :ssn_last_four_digits)
    end

    def city_params
      params.require(:cti).permit(:name, :latitude, :longitude, :google_place_id)
    end

    def tag_params
      params.permit({:tags => []})
    end

    def language_params
      params.permit({:languages => []})
    end

    # def image_params
    #   params.permit({:images => []})
    # end

    def accessed_current_user?
      if current_user == nil 
        render :template => "errors/partial/session/_nil",:formats=> :html, :status => 404
      elsif params[:id].to_i != current_user.id
        @page_title = "Login Required"
        render :template => "errors/partial/broken", :status => 403 
      end
    end
end







