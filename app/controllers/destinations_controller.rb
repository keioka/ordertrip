class DestinationsController < ApplicationController
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }
  def create 
    render :json=>true
  end

  def google
    place_data = params["place"]
    response = Destination.create_google_destination(place_data)
    render json: response
  end 

  def foursquare  
    foursquare_client_id = Settings.foursquare.foursquare_client_id
    foursquare_client_secret = Settings.foursquare.foursquare_client_secret
    #Four Square
    base_url = "https://api.foursquare.com/v2/venues/search?categoryId=4bf58dd8d48988d1e1931735&client_id=#{foursquare_client_id}&client_secret=#{foursquare_client_secret}&v=20130815%20&near="
    city = params[:name]

    city = city.split(" ").join("+")
    foursquare_response = HTTParty.get(base_url + city)
    p foursquare_response
    

    places = []

    foursquare_response["response"]["venues"].each do |v|
      place_id = v["id"]
      name = v["name"]
      address = v["location"]["formattedAddress"]
      latitude = v["location"]["lat"]
      longitude = v["location"]["lng"]


      destination = Destination.find_or_initialize_by(:provider_place_id => place_id, :provider => "foursquare", :name => name, address: address)
        
      if destination.new_record?
      
        destination.create_geolocation(latitude: latitude, longitude: longitude)

        destination.save


        photos = HTTParty.get("https://api.foursquare.com/v2/venues/#{v["id"]}/photos/?&client_id=#{foursquare_client_id}&client_secret=#{foursquare_client_secret}&v=20130815%20")     

        photo_urls = []

        if !photos.empty?

          image_urls = photos["response"]["photos"]["items"]

          image_urls.each do |image|
            url = image["prefix"] + "300x500" + image["suffix"]
            destination.image_urls.create(url: url);
          end
        end
        destination.save
      end
      obj = {
        destination: destination,
        geolocation: destination.geolocation,
        image_urls: destination.image_urls.map{|des| des.url}
      }
      places << obj
    end
    render json: places
    end
end
