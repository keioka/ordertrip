class ChargesController < ApplicationController
  
  def create
    # Amount in cents
      # if it is not successed, return error messages
      #if charge is successed
      
      # ============= stipe starts ============
      begin
        customer = Stripe::Customer.create(
         :email => current_user.email,
         :card  => params[:stripeToken]
        )

        charge = Stripe::Charge.create(
          :customer    => customer.id,
          :amount      => params[:price].to_i * 100,
          :description => params[:description],
          :currency    => 'usd'
        )

        # ============= stripe end ============= 
        begin
          Charge.create(sender_id: current_user.id, receiver_id: params[:receiver], price: params[:price].to_i, offer_id: params[:offer_id])
          offer = Offer.find(params[:offer_id])
          offer.accepted!(params[:receiver]) #offer.rb
          send_offer_accepted_mail(params[:offer_id], params[:receiver], current_user.id) #application_controller.rb
        rescue => e
          logger.error(e)
        end
        flash[:success] = "Your payment is successed"  
        redirect_to "/dashboard/transaction"
      rescue Stripe::CardError => e
        flash[:error] = "Your payment is not processed."
        logger.warn("[Stripe Error] " +e.message)
        redirect_to :back
      end
      
  end
end
