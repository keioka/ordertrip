class OffersController < ApplicationController
  before_action :sign_in_alert
  before_action :access_validation, only: [:show, :update, :destroy, :edit, :message, :cancel]
  

  # /POST tours/:tour_id/offers

  def create
    offer = Offer.new(offer_params.merge(guide_id: current_user.id, tour_id: params[:tour_id], accepted: false ))
    if offer.save  
      redirect_to :back
    else
      redirect_to :back
    end
  end
  

  # /GET tours/:tour_id/offers/:id 

  def show
    @tour = Tour.find(params[:tour_id])
    begin
      @offer = Offer.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render :template => "errors/session/_notfound"
    end
  end

  # /GET tours/:tour_id/offers/:id 

  def update
  end

  def destroy
  end

   # /POST tours/:tour_id/offers/:id /messages

  def message
    offer = Offer.find(params[:id])
    message = offer.messages.build(message_params)
    message.sender_id = current_user.id
    traveler_id = Tour.find(params[:tour_id]).traveler_id
    if current_user.id == traveler_id 
      message.receiver_id = offer.guide_id
    else
      message.receiver_id = traveler_id
    end

    if message.save
      send_messages_notification(params[:id], message.sender_id, message.receiver_id, "/tours/#{params[:tour_id]}/offers/#{params[:id]}")
      redirect_to :back
    else
      redirect_to :back
    end
    # message.receiver_id = 
  end

  def cancel
    offer = Offer.find(params[:id])
    offer.cancel!
    render json: { "message": "success" }
  end

  def review
    offer = Offer.find(params[:id])
    if current_user.id == offer.guide_id
      reviewee = offer.tour.traveler_id
    elsif current_user.id == offer.tour.traveler_id
      reviewee = offer.guide_id
    end
    reviewer = current_user.id
    offer.review_post(params[:review][:body], reviewer, reviewee)
    redirect_to dashboard_path
  end

  private
    def offer_params
      params.require(:offer).permit(:price, :message)
    end

    def tour_id
      params.permit(:tour_id)
    end

    def message_params
      params.require(:message).permit(:body)
    end 

    def sign_in_alert
      if session_nil?
       render :template => "errors/partial/session/_nil"
      end
    end

    def access_validation
      tour_id = params[:tour_id]
      offer_id = params[:id]
      traveler_id = Tour.find(tour_id).traveler_id
      guide_id = Offer.find(offer_id).guide_id

      unless guide_id == current_user.id || traveler_id == current_user.id
        render :template => "errors/offers/offer_private_error"
      end
    end
end







