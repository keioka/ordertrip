class AuthsController < ApplicationController

  def facebook
    
    response = request.env['omniauth.auth']
    facebook_uid = response.uid
    facebook_login = FacebookLogin.find_or_initialize_by(facebook_uid: facebook_uid)
    user = User.find_by(facebook_login_id: facebook_login.id) unless facebook_login.id.nil?
    
    # if user is exsist    
    if user 
    
      sign_in(user, "Facebook")
      current_user 
    
      begin
        redirect_to :back
      rescue ActionController::RedirectBackError => e 
        logger.warn(e)
        redirect_to root_path
      end

    elsif facebook_login.new_record? 
      
      if facebook_login.save
       
        user = facebook_login.create_facebook_login(response)
          
        if user 
          send_welcome_email(user)
          flash[:success] = "Welcome to OrderTrip!"
          sign_in(user, "Facebook")
          current_user 
          redirect_to edit_user_path(user)
        else
       
          begin
            logger.warn("Facbook login user is not created &" + user.errors.messages )
            redirect_to :back
          rescue ActionController::RedirectBackError => e 
            logger.warn(e)
            redirect_to root_path
          end
       
        end
      else
        logger.warn("Facebook Login account is not created!")
      end



    elsif user.nil?
      
      
      if facebook_login.save
    
       
        user = facebook_login.update_facebook_login_user(response)
      
    
        if user 
          
          sign_in(user, "Facebook")
          current_user 
          redirect_to "/users/" + session[:user_id].to_s + "/edit"
    
        else

          begin
            logger.warn("Facebook login user is not created" )
            flash[:error] = "Something wrong while Facebook Authorization."
            redirect_to :back
            # redirect back error
          rescue ActionController::RedirectBackError => e 
            logger.warn(e)
            redirect_to root_pat 
          end
    
        end

      end
    end
          
  end
end
