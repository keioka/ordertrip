class LanguagesController < ApplicationController
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }
  def index
    @languages = Language.all.pluck(:name)
    render json: @languages
  end 
end
