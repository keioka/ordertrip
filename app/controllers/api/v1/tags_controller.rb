class Api::V1::TagsController < Api::V1::ApplicationController
  def index
    tag = Tag.all
    render json: tag.to_json
  end

  def create
    # Tag.create()
  end

  private 
    def tag_params
      params.require(:tags)
    end
end
