class Api::V1::CitiesController  < Api::V1::ApplicationController
  def index
    cities = City.all 
    render json: cities.to_json
  end

  def update
    city = City.find(city_params[:id])
    begin 
      city.update_attributes(city_params)
      city.geolocation.update_attributes(latitude: city_params["latitude"], longitude: city_params["longitude"] )
    rescue => e
      render json: {         
        status: 400, 
        message: e
      }
    end
  end

  private 
    def city_params
      params.require(:city).permit(:id, :name, :latitude, :longitude, :google_place_id) 
    end
end
