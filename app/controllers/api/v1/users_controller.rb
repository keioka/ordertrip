class Api::V1::UsersController < Api::V1::ApplicationController
  def index
    users = User.all
    render json: users.to_json(:include => :city) 
  end

  def show
    user = User.find(params[:id])
    render json: user.to_json(:include => [:charged, :charging, :tours, :offers]) 
  end
end
