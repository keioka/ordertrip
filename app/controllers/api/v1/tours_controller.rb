class Api::V1::ToursController < Api::V1::ApplicationController
  def index
    @tours = Tour.all
    render json: @tours.includes(:offers)
  end 
end
