class Api::V1::OffersController < Api::V1::ApplicationController
  def index
    @offers = Offer.all
    render json: @offers.to_json(:include => [:guide, :tour]) 
  end
end
