class Api::V1::ChargesController < Api::V1::ApplicationController
  def index
    @charges = Charge.all
    render json: @charges.to_json 
  end
end
