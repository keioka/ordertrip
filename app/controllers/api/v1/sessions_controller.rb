class Api::V1::SessionsController < Api::V1::ApplicationController
  def facebook
    facebook_logins = FacebookLogin.all
    render json: facebook_logins.to_json
  end

  def email
    email_logins = EmailLogin.all
    render json: email_logins.to_json
  end
end