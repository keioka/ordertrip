class Api::V1::DestinationsController  < Api::V1::ApplicationController
  def index
    destinations = Destination.all 
    render json: destinations.includes(:geolocation).to_json
  end

  def update
    des = Destination.find(destination_params["id"])
    begin 
      des.update_attributes(destination_params)
    rescue => e
      render json: {         
        status: 400, 
        message: e
      }
    end
  end

  private 
    def destination_params
      params.require(:destinations).permit(:id, :provider_place_id, :provider, :name, :address, :rating, :google_place_id) 
    end
end
