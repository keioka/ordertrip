class Api::V1::ApplicationController < ApplicationController
  before_action :check_access_code
  def check_access_code
    if params[:admin_key] == nil
      render json: { status:403 }, status: :forbidden 
    elsif  params[:admin_key] != Rails.application.secrets.admin_key
      render json: { status:403 }, status: :forbidden
    end
  end
end
