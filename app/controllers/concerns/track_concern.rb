module TrackConcern
  extend ActiveSupport::Concern

  def new_session?(last_session_time)
    date = DateTime.parse(last_session_time)
    date < 30.minutes.ago
  end

  def get_random_session_key
    SecureRandom.urlsafe_base64(8)
  end

  # def get_city(parmas)
  #   city_array = make_city_array(parmas)
  #   if city_array.length > 1
  #     city = {
  #       city: city_array.first
  #       country: city_array.last
  #     }
  #     city
  #   else
  #     city_name = city_array.first
  #   end
  # end


# included do
#   has_many :taggings, as: :taggable, dependent: :destroy
#   has_many :tags, through: :taggings
# end

# def tag_names
#  tags.map(&:name)
# end
end