class CitiesController < ApplicationController
  def index
    @tours = Tour.limit(6).order(created_at: :desc)
  end

  def create
    # city_parmas_array = make_city_array(city_params)

    # city_name = city_parmas_array.first
    # country = city_parmas_array.last

    # city = City.find_or_initialize_by(name: city_name)

    # if city.new_record?
    #   city.country = country
    #   city.build_geolocation(latitude: city_params["latitude"], longitude: city_params["longitude"]) 
    #   city.save
    # end
  end

  def show
    @city = City.includes(:geolocation).find_by(city_name_id: params[:id])
    @tours = Tour.where(city_id: @city.id)
  end

  def locals 
    # city_id = City.find_by(name: params[:id]).id
    # @users = User.find_by(city_id: city_id)

    #Haversine formula
    # $sql = "SELECT *, ( 3959 * acos( cos( radians(" . $lat . ") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(" . $lng . ") ) + sin( radians(" . $lat . ") ) * sin( radians( lat ) ) ) ) AS distance FROM your_table HAVING distance < 5";
    # http://stackoverflow.com/questions/28973253/how-to-search-nearby-places-stored-in-my-database
    @city = City.find_by(city_name_id: params[:id])
    @users = User.where(city_id: @city.id)
    render "cities/locals"
  end 

  def locals_locations
    # @geolocations = User.all.map do |user| 
    #   if user.city_id
    #     city = City.find(user.city_id)
    #     if city
    #       city.geolocation 
    #     else
    #       ""
    #     end
    #   end
    # end
    @geolocations = City.joins(:users, :geolocation).distinct.map{|l| [l.geolocation.latitude,l.geolocation.longitude] }
    render json: @geolocations
  end  
end
