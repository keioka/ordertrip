class ToursController < ApplicationController

  def create 
    city = City.create_city_from_params(city_params)
    # create_city(city_params) 
    tour = Tour.new(tour_params.merge(traveler_id: current_user.id, city_id: city.id))
    if tour.save
      unless params["destinations"].nil?
        tour_destinations(tour, destination_params)
      else
        logger.warn("Destinations are not gotten")
      end
      flash[:success] = "Your trip is posted"
      render json: { status: 201 }
    else
      messages = tour.errors.full_messages.join(". ")
      logger.warn(messages)
      render json: { status: 422, message: messages }, :status => 422
    end 
  end

  def new 
  end

  def show
    @tour = Tour.find(params[:id])
    @city = City.find(@tour.city_id)
    set_seo_elements(@tour.title, @city.name)
  end

  def update
  end

  def destroy
    tour = Tour.find(params[:id])
    unless editable?(tour) == true && tour.offers.nil?
    tour.destroy
      render json: { message: "Success" }
    end
    render json: { message: "Error" }
  end

  private 
    def tour_params
      params.require(:tour).permit(:tour_date, :start_time, :end_time, :budget,:title, :description )
    end

    def city_params
      params.require(:city).permit(:name, :latitude, :longitude, :google_place_id) 
    end

    def destination_params
      params.permit({:destinations => []})
    end

end
