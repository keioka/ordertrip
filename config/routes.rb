Rails.application.routes.draw do
  
  # # # # # # # # # Logger # # # # # # # # # # # #
  mount RailsClientLogger::Engine, :at => "logger"
  # mount Peek::Railtie => '/peek'

  scope "(:locale)", locale: /#{I18n.available_locales.join("|")}/ do

  #match '*path', to: redirect("%{path}/?locale=#{I18n.default_locale}"), via: [:get]


  # # # # # # # # # Root # # # # # # # # # # # #
  
  root 'cities#index'
  

  # # # # # # # # # Campaign Redirect # # # # # # # # # # # #
  get 'instagram' => 'campaign#instagram'
  get 'twitter' => 'campaign#twitter'
  get 'facebook' => 'campaign#facebook'
  get 'hello' => 'campaign#cp01'
  get 'ahoy' => 'campaign#cp02'


  # # # # # # # # # Static Pages # # # # # # # # # # # #
  get 'mission' => 'static_pages#mission'
  get 'payment' => 'static_pages#payment'
  get 'team' => 'static_pages#team'
  get 'terms' => 'static_pages#terms'


  # # # # # # # # # Json # # # # # # # # # # # #
  get 'base' => 'base#index'
  get 'tags' => 'tags#index'
  get 'languages' => 'languages#index'
  

  # # # # # # # # # Auth Callback # # # # # # # # # # # #
  get 'auth/facebook/callback' => 'auths#facebook' 
  

  # # # # # # # # # Dashboard # # # # # # # # # # # #
  get 'dashboard' => 'dashboards#index'
  get 'dashboard/reviews' => 'dashboards#review'
  get 'dashboard/offers' => 'dashboards#offer'
  get 'dashboard/orders' => 'dashboards#order'
  get 'dashboard/transaction' => 'dashboards#transaction'
  post 'dashboard/tours/:id/reviews' => 'dashboards#review'
  
  
  # # # # # # # # # User # # # # # # # # # # # #
  resources :users, only: [:create, :edit, :show, :update, :destroy] do
  end
  
  # # # # # # # # # Session # # # # # # # # # # # #
  resources :sessions, only: [:create] do
    collection do 
      delete 'logout'
      get 'confirm_email'      
      get 'request_new_password'
      post 'send_reset_token'
      get 'set_new_password'
      post 'reset_password'
    end
  end
  

  # # # # # # # # # Destination # # # # # # # # # # # #
  resources :destinations, only: [:create] do 
    collection do 
      post 'foursquare'
      post 'google'
    end
  end


  # # # # # # # # # Charge # # # # # # # # # # # #
  resources :charges, only: [:create] do 
  end


  # # # # # # # # # Cities # # # # # # # # # # # #
  resources :cities, param: :city_name_id, only: [:show, :create] do
    member do 
      get 'locals'
    end
    collection do 
      get 'locals_locations'
    end
  end

  
  # # # # # # # # # Tours Offers # # # # # # # # # # # #
  resources :tours, only: [:create, :new, :show, :update, :destroy] do
    resources :offers, only: [:create, :show, :update, :destroy] do 
      member do
        post 'message'
        post 'review'
        get 'cancel'
      end
    end
  end

  
  
end



  # # # # # # # # # API v1  # # # # # # # # # # # #
  namespace :api do
    namespace :v1 do
      resources :sessions, only: [:index] do
        collection do
          get 'facebook'
          get 'email'
          # get 'cancel'
        end
      end
      resources :users, only: [:index, :show] do
      end
      resources :tours, only: [:index] do
      end
      resources :offers, only: [:index] do
      end
      resources :charges, only: [:index] do
      end
      resources :cities, only: [:index, :create, :update] do
      end
      resources :destinations, only: [:index, :create, :update] do
      end
    end
  end
# match '*path', to: redirect("/#{I18n.default_locale}/%{path}"), via: [:get]
#  match '', to: redirect("/#{I18n.default_locale}/%{path}"), via: [:get]
#

  # # # # # # # # # # # # # # # # # # # # # # # # #

  # root 'layouts#index'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"


  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # Catching all URL for routing error
  match "*path" => "errors#handel_404", via: [ :get, :post, :patch, :delete ]

end
