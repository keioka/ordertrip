set :application, 'ordertrip'
set :repo_url, 'git@bitbucket.org:keioka/ordertrip.git'
set :deploy_to, '/var/www/ordertrip'
set :keep_releases, 5 #何個アプリを確保しておくか。この場合はデプロイした最新のアプリ5個をキープ
set :rbenv_type, :user
set :rbenv_ruby, '2.2.2'    #rubyのバージョン間違えないように!
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles, :all
set :linked_dirs, %w{log tmp/backup tmp/pids tmp/cache tmp/sockets vendor/bundle}
set :assets_roles, [:web, :app]
set :bundle_flags, "--quiet"


# Additional

set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }
set :whenever_roles, ->{ :batch }
set :whenever_command, 'bundle exec whenever'

namespace :deploy do
  task :restart do
    invoke 'unicorn:restart'
  end

  desc 'db_seed'
  task :db_seed do
    on roles(:db) do |host|
      with rails_env: fetch(:rails_env) do
        within current_path do
          execute :bundle, :exec, :rake, 'db:seed'
        end
      end
    end
  end

  after 'deploy:publishing', 'deploy:restart'

  after :restart, :clear_cache do
  on roles(:web), in: :groups, limit: 3, wait: 10 do
    # Here we can do anything such as: 
    # within release_path do 
    #   execute :rake, 'cache:clear'
    # end
    end
  end
end

namespace :sitemap do
  desc 'generate'
  task :generate do
    on roles(:app) do
      execute :bundle, :exec, :rake, 'sitemap:generate'
    end
  end
end

# after 'deploy:finished', 'sitemap:generate'


# worker_processes 2
# listen "/tmp/unicorn.sock"
# pid "/tmp/unicorn.pid"

# #timeout 60

# preload_app true

# stderr_path File.expand_path('unicorn.log',   File.dirname(__FILE__) + '/../log')
# stdout_path File.expand_path('unicorn.log',   File.dirname(__FILE__) + '/../log')

# before_fork do |server, worker|
#   old_pid = "#{server.config[:pid]}.oldbin"
#   if old_pid != server.pid
#     begin
#       sig = (worker.nr + 1) >= server.worker_processes ? :QUIT : :TTOU
#       Process.kill(sig, File.read(old_pid).to_i)
#     rescue Errno::ENOENT, Errno::ESRCH
#     end
#   end
# end
# GC.respond_to?(:copy_on_write_friendly=) and GC.copy_on_write_friendly = true
