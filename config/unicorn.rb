#  -----------------------------------------------
#  TOSHIKI 10/17 comment out
#  -----------------------------------------------
#  # SHARED_PATH = "/var/www/ordertrip/shared/"
#  current_path = "/var/www/ordertrip/current"
#  
#  worker_processes 2
#  
#  # listen File.expand_path('tmp/sockets/unicorn.sock', SHARED_PATH)
#  # pid File.expand_path('tmp/pids/unicorn.pid', SHARED_PATH)
#  listen "/tmp/unicorn.sock"
#  pid "/var/www/ordertrip/shared/tmp/pids/unicorn.pid"
#  
#  
#  # listen "/tmp/sockets/unicorn.sock"
#  # pid "/tmp/pids/unicorn.pid"
#  working_directory current_path
#  timeout 60
#  
#  # ダウンタイムなくす
#  
#  preload_app true
#  
#  stderr_path File.expand_path('unicorn.stderr.log',   File.dirname(__FILE__) + '/../log')
#  stdout_path File.expand_path('unicorn.stdout.log',   File.dirname(__FILE__) + '/../log')
#  
#  before_fork do |server, worker|
#    ENV['BUNDLE_GEMFILE'] = File.expand_path('Gemfile', current_path)
#    old_pid = "#{server.config[:pid]}.oldbin"
#    if old_pid != server.pid
#      beginp
#        sig = (worker.nr + 1) >= server.worker_processes ? :QUIT : :TTOU
#        Process.kill(sig, File.read(old_pid).to_i)
#      rescue Errno::ENOENT, Errno::ESRCH
#      end
#    end
#  end
#  # GC.respond_to?(:copy_on_write_friendly=) and GC.copy_on_write_friendly = true
#  after_fork do |server, worker|
#    defined?(ActiveRecord::Base) and ActiveRecord::Base.establish_connection
#  end
app_path = "/var/www/ordertrip"
working_directory "/var/www/ordertrip/current"

worker_processes 2

listen "#{app_path}/shared/tmp/sockets/unicorn.sock"
pid "#{app_path}/current/tmp/pids/unicorn.pid"

timeout 60

preload_app true
stderr_path "#{app_path}/shared/log/unicorn.stderr.log"
stdout_path "#{app_path}/shared/log/unicorn.stdout.log"

before_exec do |server|
  ENV['BUNDLE_GEMFILE'] = "#{app_path}/current/Gemfile"
end

before_fork do |server, worker|
  old_pid = "/var/www/ordertrip/current/tmp/pids/unicorn.pid.oldbin"
  if old_pid != server.pid
    begin
      sig = (worker.nr + 1) >= server.worker_processes ? :QUIT : :TTOU
      Process.kill(sig, File.read(old_pid).to_i)
    rescue Errno::ENOENT, Errno::ESRCH
    end
  end
end
# GC.respond_to?(:copy_on_write_friendly=) and GC.copy_on_write_friendly = true
