#  -----------------------------------------------
#  Toshiki 10/17 comment out
#  -----------------------------------------------
#  set :branch, 'test'
#  set :stage, :production
#  set :bundle_binstubs, nil
#  
#  # Roles for each
#  role :app, %w{ordertrip@54.183.3.191}
#  role :web, %w{ordertrip@54.183.3.191}
#  role :db,  %w{ordertrip@54.183.3.191}
#  
#  # Detailed setting
#  server '54.183.3.191', user: 'ordertrip', roles: %w{web app db batch worder}
#  
#  # SSH options
#  set :ssh_options, {
#     keys: [File.expand_path('~/.ssh/ordertrip.pem')],
#     forward_agent: true,
#     auth_methods: %w(publickey)
#  }
#  
#  # Rails Server ENV
#  set :rails_env, 'production'
#  set :unicorn_rack_env, "production"

# Rails Server ENV
set :rails_env, 'production'
set :unicorn_rack_env, "production"
set :branch, 'production'
set :stage, :production
set :bundle_binstubs, nil

# Roles for each
role :app, %w{ordertrip@52.53.175.223}
role :web, %w{ordertrip@52.53.175.223}
role :db,  %w{ordertrip@52.53.175.223}

# Detailed setting
server '52.53.175.223', user: 'ordertrip', roles: %w{web app db batch worder}

# SSH options
set :ssh_options, {
   keys: [File.expand_path('~/.ssh/ordertrip.pem')],
   forward_agent: true,
   auth_methods: %w(publickey)
}
