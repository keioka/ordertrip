set :branch, 'staging'
set :stage, :staging
set :bundle_binstubs, nil
set :rails_env, 'staging'
set :unicorn_rack_env, "staging"

# Roles for each
role :app, %w{ordertrip@54.67.90.253}
role :web, %w{ordertrip@54.67.90.253}
role :db,  %w{ordertrip@54.67.90.253}

# Detailed setting
server '54.67.90.253', user: 'ordertrip', roles: %w{web app db batch worder}

# SSH options
set :ssh_options, {
   keys: [File.expand_path('~/.ssh/ordertrip-test')],
   forward_agent: true,
   auth_methods: %w(publickey)
}


# Rails Server ENV
