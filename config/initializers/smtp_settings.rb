ActionMailer::Base.delivery_method = :smtp
ActionMailer::Base.perform_deliveries = true
ActionMailer::Base.smtp_settings = {
    :address => "smtp.gmail.com",
    :port => 587,
    :domain => "ordertrip.com",
    :user_name => Settings.email.account,
    :password => Settings.email.password,
    :authentication => :plain,
    :enable_starttls_auto => true
}