Rails.application.config.middleware.use OmniAuth::Builder do
  # configure do |config|
  #   config.path_prefix = '/auth'
  # end
  provider :facebook, Settings.facebook.facebook_key, Settings.facebook.facebook_secret,:scope => 'email,user_likes, user_education_history, user_likes, user_location, user_photos, user_work_history, public_profile, user_friends', info_fields: 'email, name, first_name, last_name, birthday, gender, interested_in,languages,location', image_size: {width: 600}
end